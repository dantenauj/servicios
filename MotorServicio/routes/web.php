<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
		return view('frontend.bienvenida');
	});

Route::get('/home', function () {
		return view('backend.template.app');
	});
Route::post('sesion', [
		'as'   => 'login-post',
		'uses' => 'Auth\AuthController@postLogin',
	]);
Route::get('sesion', [
		'as'   => 'cerrar',
		'uses' => 'Auth\AuthController@Login',
	]);

Route::group(array('middleware' => 'auth'), function () {
		Route::resource('Acceso', 'admin\gbAccesoController');
		Route::resource('Persona', 'admin\gbPersonaController');
		Route::resource('Usuario', 'admin\gbUsuarioController');
		Route::resource('Rol', 'admin\gbRolController');
		Route::resource('Grupo', 'admin\gbGrupoController');
		Route::resource('Opcion', 'admin\gbOpcionController');
		Route::resource('Asignacion', 'admin\gbAsignacionController');
		Route::resource('RolUsuario', 'admin\gbRolUsuarioController');
		Route::get('lenguaje', 'MenuController@lenguaje');

		/***** MOTOR SERVICIOS *****/
		Route::resource('motor-servicios', 'parametricas\motorServiciosController');
		Route::resource('reglas-negocio', 'parametricas\reglasNegocioController');
		Route::resource('Servicios', 'servicios\ServiciosController');
		Route::resource('parametro', 'parametricas\catalogoController');

		Route::get('v1_0/motorServicios/getRestDinamico/{id}', 'parametricas\motorServiciosController@get_restDinamico');
		Route::get('v1_0/motorServicios/lst_motor_servicios', 'parametricas\motorServiciosController@lst_motor_servicios');
		Route::post('v1_0/motorServicios/darBajaMotorServicio', 'parametricas\motorServiciosController@darBajaMotorServicio');
		Route::post('v1_0/motorServicios/actualizarServicio', 'parametricas\motorServiciosController@actualizarServicio');
		Route::post('v1_0/motorServicios/servicioSP', 'parametricas\motorServiciosController@get_restDinamico_servicio');
		Route::post('v1_0/motorServicios/insertarServicio', 'parametricas\motorServiciosController@insertarServicio');

		Route::get('v1_0/motorServicios/ejcutar_REST_API', 'parametricas\motorServiciosController@get_restDinamico_CURL');
		Route::get('v1_0/motorServicios/get_Dato_Servicio/{id}', 'parametricas\motorServiciosController@get_servicioID');
		Route::get('v1_0/motorServicios/llama_Conn', 'parametricas\motorServiciosController@llamarConexionDB');

		Route::get('v1_0/motorServicios/lst_workspace', 'parametricas\motorServiciosController@lst_catalogo');
		Route::post('v1_0/motorServicios/lst_servicios1', 'parametricas\motorServiciosController@lst_servicio1');

		/****** Reglas de Negocio *****/
		Route::post('v1_0/reglanegocio/editarCodigo', 'parametricas\reglasNegocioController@editarCodigo');
		Route::get('v1_0/reglaNegosio/get_Codigo/{id}', 'parametricas\reglasNegocioController@get_restCodigo');

		Route::get('v1_0/reglaNegocio/lst_reglasNegocio', 'parametricas\reglasNegocioController@lst_reglasNegocio');
		Route::post('v1_0/reglaNegocio/lst_reglasNegocio1', 'parametricas\reglasNegocioController@lst_reglasNegocio1');

		Route::post('v1_0/reglaNegocio/nuevo_registro', 'parametricas\reglasNegocioController@insertarRegla');
		Route::post('v1_0/reglaNegocio/darBajaRegla_Negocio', 'parametricas\reglasNegocioController@darBajaRegla_Negocio');
		Route::get('v1_0/reglaNegocio/get_Combo', 'parametricas\reglasNegocioController@lst_reglasNegocioCombo');
		Route::post('v1_0/reglaNegocio/up_DataCodigo', 'parametricas\reglasNegocioController@editarDataCodigo');
		Route::post('v1_0/reglaNegocio/coneccionDB', 'parametricas\reglasNegocioController@conexionMongoDB');
		Route::get('v1_0/reglaNegocio/get_datos_Codigo/{id}', 'parametricas\reglasNegocioController@get_reglanegocioID');

		Route::get('v1_0/reglanegocio/ejecutar_REST', 'parametricas\reglasNegocioController@ejecutarREST');
		Route::get('v1_0/reglaNegocio/ejecutar_Query', 'parametricas\reglasNegocioController@ejecutarQuery');
		Route::get('v1_0/reglanegocio/ejecutar_Conn', 'parametricas\reglasNegocioController@ejecutarConexionDB');
		Route::get('v1_0/reglaNegocio/get_ServiciosRS/{tipo}', 'parametricas\reglasNegocioController@lst_reglasServicioCombo');
		Route::post('v1_0/reglaNegocio/conexion_DB', 'parametricas\reglasNegocioController@get_conexionDB');
		Route::get('v1_0/reglanegocio/ejecutarPaso', 'parametricas\reglasNegocioController@ejecutar_unitario');
		Route::post('v1_0/reglaNegocio/identificador', 'parametricas\reglasNegocioController@identificador');
		//Route::post('v1_0/reglaNegocio/ejecutarWeb','parametricas\reglasNegocioController@ejecutarWeb');

		Route::get('inicio', function () {
				echo 'Bienvenido ';

				// Con la función Auth::user() podemos obtener cualquier dato del usuario
				// que este en la sesión, en este caso usamos su correo y su id
				// Esta función esta disponible en cualquier parte del código
				// siempre y cuando haya un usuario con sesión iniciada
				echo 'Bienvenido '.Auth::user()->usr_usuario.', su Id es: '.Auth::user()->usr_id;
			});
		Route::get('close', [
				'as'   => 'cerrar',
				'uses' => 'Auth\AuthController@close',
			]);

	});