<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Credentials: true");
header("Access-Control-Max-Age: 1000");
header("Access-Control-Allow-Headers: X-Requested-With, Content-Type, Origin, Cache-Control, Authorization, Accept, Accept-Encoding, If-Modified-Since, User-Agent");
header("Access-Control-Allow-Methods: PUT, POST, GET, OPTIONS");

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::middleware('auth:api')->get('/user',

function (Request $request) {
		return $request->user();
	});

Route::get('apiLogin', [
		'as'   => 'api-login',
		'uses' => 'servicios\ApiAuthController@postLogin'
	]);
Route::post('apiLogin', [
		'as'   => 'api-login',
		'uses' => 'servicios\ApiAuthController@postLogin'
	]);

Route::post('apiCrearUsuario', [
		'as'   => 'api-crear',
		'uses' => 'servicios\ApiAuthController@crear_usuario'
	]);

Route::group(array('middleware' => 'jwt.auth'), function () {
		Route::post('reglaNegocio/ejecutarWeb', 'servicios\ServiciosController@ejecutarWeb');
		Route::get('apiCerrar', [

				'as'   => 'cerrarApi',
				'uses' => 'servicios\ApiAuthController@logout'
			]);
	});


