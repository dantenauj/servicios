<?php

namespace gamlp\Modelo\parametricas;

use Illuminate\Database\Eloquent\Model;
use Yajra\Datatables\Datatables;
use Alert;

class Catalogo extends Model
{
    protected $table      = 's_catalogo';
    protected $fillable   = ['ctp_id', 'ctp_descripcion', 'ctp_codigo', 'ctp_clasificador','ctp_usr_id','ctp_registrado','ctp_modificado','ctp_estado'];
    public $timestamps    = false;
    protected $primaryKey = 'ctp_id';

    protected static function getListarCatalogo()
    {
        $catalogo = Catalogo::select('ctp_id', 'ctp_descripcion', 'ctp_codigo', 'ctp_clasificador','ctp_estado')
                            ->where('ctp_estado','A')
                            ->orderBy('ctp_clasificador','ASC')
                            ->get();  
        return $catalogo;
    }
    protected static function getId(){
        $id = Catalogo::select('ctp_id')
            ->orderBy('ctp_id', 'DESC')
            ->first();
        return $id;
    }

    protected static function getDestroy($id)
    {
        $Catalogo = Catalogo::where('ctp_id', $id)->update(['ctp_estado' => 'B']);
        return $Catalogo;
    }

    protected static function getCatalogo($id)
    {
        $Catalogo = Catalogo::where('ctp_id', $id)
                        ->where('ctp_estado','A')
                        ->first();
        return $Catalogo;
    }
}