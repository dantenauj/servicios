<?php

namespace gamlp\Http\Controllers\admin;

use Auth;
use gamlp\Http\Controllers\Controller;
USE gamlp\Modelo\admin\LogSeguimiento;
use gamlp\Modelo\admin\Persona;
use gamlp\Modelo\admin\Usuario;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class gbUsuarioController extends Controller {
	public function index() {
		$persona = Persona::OrderBy('prs_id', 'desc')->pluck('prs_nombres', 'prs_id');
		return view('backend.administracion.admin.gbUsuario.index', compact('persona'));

	}
	public function create() {
		$usr = Usuario::getListar();
		return datatables::of($usr)->addColumn('acciones', function ($usuario) {
				return '<button value="'.$usuario->usr_id.'" class="btncirculo btn-xs btn-primary" style="background:#57BC90" onClick="MostrarUsuario(this);" data-toggle="modal" data-target="#myUserUpdate"><i class="fa fa-pencil-square"></i></button>
            <button value="'.$usuario->usr_id.'" class="btncirculo btn-xs btn-warning" style="background:#7ACCCE" onClick="EliminarUsuario(this);"><i class="fa fa-trash-o"></i></button>';})
			->editColumn('id', 'ID: {{$usr_id}}')	->make(true);
	}
	public function store(Request $request) {
		\DB::enableQueryLog();
		$ids = Auth::user()->usr_id;
		if ($request->ajax()) {
			Usuario::create([
					'usr_usuario' => $request['usr_usuario'],
					'usr_clave'   => bcrypt($request['usr_clave']),
					'usr_prs_id'  => $request['usr_prs_id'],
					'usr_usr_id'  => Auth::User()->usr_id,
				]);
			$consulta = \DB::getQueryLog();
			foreach ($consulta as $i => $query) {
				$resultado = json_encode($query);
			}
			\DB::disableQueryLog();
			LogSeguimiento::create([
					'log_usr_id'   => $ids,
					'log_metodo'   => 'POST',
					'log_accion'   => 'CREACION',
					'log_detalle'  => "el usuario".Auth::User()->usr_usuario."agrego un nuevo registro",
					'log_modulo'   => 'USUARIOS',
					'log_consulta' => $resultado,
				]);
			return response()->json(["Mensaje" => "Se registro Correctamente"]);
		} else {
			return response()->json(["Mensaje" => "Hubo un error al registrar los datos"]);
		}

	}

	public function show($id) {
	}

	public function edit($id) {
		$usuario = Usuario::setBuscar($id);
		return response()->json($usuario);
	}
	public function update(Request $request, $id) {

		\DB::enableQueryLog();
		$ids = Auth::user()->usr_id;
		if ($request->ajax()) {
			$usuario = Usuario::setBuscar($id);
			$usuario->fill($request->all());
			$usuario->save();
			$consulta = \DB::getQueryLog();
			foreach ($consulta as $i => $query) {
				$resultado = json_encode($query);
			}
			\DB::disableQueryLog();
			LogSeguimiento::create([
					'log_usr_id'   => $ids,
					'log_metodo'   => 'PUT',
					'log_accion'   => 'ACTUALIZACION',
					'log_detalle'  => "el usuario".Auth::User()->usr_usuario." actualizo un nuevo registro",
					'log_modulo'   => 'USUARIOS',
					'log_consulta' => $resultado,
				]);
			return response()->json(["Mensaje" => "Usuario fue actualizado", "datos" => $request->all()]);
		} else {
			return response()->json(["Mensaje" => "Hubo un error al actualizar los datos"]);
		}
	}
	public function destroy($id) {
		\DB::enableQueryLog();
		$ids      = Auth::user()->usr_id;
		$susuario = Usuario::setDestroy($id);
		$consulta = \DB::getQueryLog();
		foreach ($consulta as $i => $query) {
			$resultado = json_encode($query);
		}
		\DB::disableQueryLog();
		LogSeguimiento::create([
				'log_usr_id'   => $ids,
				'log_metodo'   => 'DELETE',
				'log_accion'   => 'ELIMINACION',
				'log_detalle'  => "el usuario".Auth::User()->usr_usuario." elimino un registro",
				'log_modulo'   => 'USUARIOS',
				'log_consulta' => $resultado,
				//'file'=>input_file($fichero)
			]);
		return response()->json(["Mensaje" => "Usuario fue eliminado"]);

	}
}
