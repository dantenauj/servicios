<?php

namespace gamlp\Http\Controllers\admin;

use gamlp\Http\Controllers\Controller;
use gamlp\Modelo\admin\Rol;
use Illuminate\Http\Request;
use Redirect;
use Yajra\Datatables\Datatables;

class gbRolController extends Controller {

	public function index() {
		$rol = Rol::getListar();
		return view('backend.administracion.admin.gbRoles.index', compact('rol'));
	}

	public function create() {
		$rol = Rol::getListar();
		return Datatables::of($rol)->addColumn('acciones', function ($rol) {
				return '<button value="'.$rol->rls_id.'" class="btncirculo btn-xs btn-primary" style="background:#57BC90" onClick="Mostrar(this);" data-toggle="modal" data-target="#myUpdate"><i class="fa fa-pencil-square"></i></button>
            <button value="'.$rol->rls_id.'" class="btncirculo btn-xs btn-warning" style="background:#7ACCCE" onClick="Eliminar(this);"><i class="fa fa-trash-o"></i></button>';
			})
			->editColumn('id', 'ID: {{$rls_id}}')
			->make(true);
	}

	public function store(Request $request) {
		Rol::create([
				'rls_rol'    => $request['rls_rol'],
				'rls_usr_id' => 1,
				'rls_estado' => 'A',
			]);
		return redirect()->route('Rol.index')->with('success', 'El rol se registro correctamente');
	}

	public function show($id) {

	}

	public function edit($id) {

		$rol = Rol::setBuscar($id);
		return response()->json($rol);
	}

	public function update(Request $request, $id) {

		$rol = Rol::setBuscar($id);

		$rol->fill($request->all());
		$rol->save();
		return response()->json($rol->toArray());
	}

	public function destroy($id) {
		$rol = Rol::getDestroy($id);
		return response()->json($rol);

	}
}