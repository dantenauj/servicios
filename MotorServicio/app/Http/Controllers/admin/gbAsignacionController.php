<?php

namespace gamlp\Http\Controllers\admin;

use Auth;
use gamlp\Http\Controllers\Controller;
use gamlp\Modelo\admin\Acceso;

use gamlp\Modelo\admin\Grupo;

use Illuminate\Http\Request;
use Session;

class gbAsignacionController extends Controller {
	public function index() {
		$rol    = Acceso::getListarRol();
		$opc    = Acceso::getListarOpcion();
		$acceso = Acceso::getListar();
		return view('backend.administracion.admin.gbAsignacion.index', compact('rol', 'opc', 'acceso'));
	}

	public function create() {
		return $grupillo = Grupo::all();
	}

	public function store(Request $request) {

		$valor_boton1 = $request['rolos'];
		if (isset($_POST['opciones'])) {
			$arr_opc = $_POST['opciones'];
			for ($i = 0; $i < count($arr_opc); $i++) {
				Acceso::create([
						'acc_rls_id' => $request['rolos'],
						'acc_opc_id' => $arr_opc[$i],
						'acc_usr_id' => Auth::User()->usr_id,
					]);
			}
			$rol    = Acceso::getListarRol();
			$opc    = Acceso::getListarOpcionParam($valor_boton1);
			$acceso = Acceso::getListarAccesoParam($valor_boton1);
			return view('backend.administracion.admin.gbAsignacion.index', compact('rol', 'opc', 'acceso'));
		} else {
			$arr_acc = $_POST['asignaciones'];
			for ($i = 0; $i < count($arr_acc); $i++) {
				Acceso::where('acc_id', $arr_acc[$i])
					->update(['acc_estado' => 'B']);
			}
			$rol    = Acceso::getListarRol();
			$opc    = Acceso::getListarOpcionParam($valor_boton1);
			$acceso = Acceso::getListarAccesoParam($valor_boton1);
			return view('backend.administracion.admin.gbAsignacion.index', compact('rol', 'opc', 'acceso'));
		}
	}

	public function show($id) {

		$grupo = Grupo::where('grp_id', $id)->update(['grp_estado' => 'B']);
		Session::flash('message', 'Grupo eliminado correctamente');
		$grupillo = Grupo::getListar();
		Session::flash('message', 'El grupo fue eliminado correctamente.');
		return view('admin.gbGrupos.read', compact('grupo', 'grupillo'));
	}
	public function edit($id) {
		$grupo    = Grupo::setBuscar($id);
		$grupillo = Grupo::getListar();
		return view('admin.gbGrupos.update', compact('grupo', 'grupillo'));
	}

	public function update(Request $request, $id) {
		$grupo = Grupo::setBuscar($id);
		$grupo->fill($request->all());
		$grupo->save();
		$grupillo = Grupo::getListar();
		Session::flash('message', 'El grupo se editado Correctamente');
		return view('admin.gbGrupos.read', compact('grupillo'));
	}

	public function destroy($id) {

	}
}
