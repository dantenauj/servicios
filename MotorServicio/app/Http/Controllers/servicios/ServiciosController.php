<?php

namespace gamlp\Http\Controllers\servicios;
use gamlp\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Ixudra\Curl\Facades\Curl;
use PDO;
use stdClass;
use MongoDB\Client as Mongodb;
class ServiciosController extends Controller {

    public function ejecutarWeb(Request $request) {
        
        //dd($request["parametros"]); 
        $previoLib = ''.
        'use Illuminate\Support\Collection;
        use MongoDB\Client as Mongodb;';
        $previoPrevio = '
        $_PARAMETROS = []; 
        $_PARAMETROS = json_decode(\''.self::filterXSS(strip_tags($request["parametros"])).'\',true);
        if(sizeof($_PARAMETROS)!=0) {foreach($_PARAMETROS as $titulo=>$valor){ $x = $titulo;
            $$x= $valor;}}';
        $previo = ''.
        'static $conn=array();
        function verifJson($valor) {
            echo($valor);
            return $valor;
        }
        function ejecutar_Conn($id) {
            $data = \DB::select("select * from sp_obtener_servicio(?)",array($id));
            try {
                $dbConn =getDB2($data[0]->_ser_gestor,$data[0]->_ser_host,$data[0]->_ser_usuario,$data[0]->_ser_clave,$data[0]->_ser_bd,$data[0]->_ser_puerto);
                return $dbConn;
            } catch (PDOException $e) {
                            die("Sin conecxión: " . $e->getMessage());
                            return json_encode(fale);
            }
        }
        function coneccionMDB($tipo,$nombrebd,$host,$puerto) {
            switch ($tipo){
                case "postgres":
                    $dbConnection = new PDO("pgsql:dbname=$nombrebd;host=$host;port=$puerto", $usuario, $clave);    
                break;
                case "mssql":
                    $dbConnection = new PDO("dblib:host=$host;dbname=$nombrebd", $usuario, $clave);
                break;
                case "mysql":
                    $dbConnection = new PDO("mysql:host=$host;dbname=$nombrebd;port=$puerto", $usuario, $clave);    
                break;
                case "mongodb":
                    $dbConnection = new Mongodb("mongodb://$host"); 
                break;
            }
            if($tipo == "mongodb"){
                $dbConnection = $dbConnection->$nombrebd;
                return $dbConnection;
             }else{
                $dbConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                return $dbConnection; 
            }  
        };
        function coneccionDB($tipo,$nombrebd,$host,$puerto,$usuario,$clave) {
            switch ($tipo){
                case "postgres":
                    $dbConnection = new PDO("pgsql:dbname=$nombrebd;host=$host;port=$puerto", $usuario, $clave);    
                break;
                case "mssql":
                    $dbConnection = new PDO("dblib:host=$host;dbname=$nombrebd", $usuario, $clave);
                break;
                case "mysql":
                    $dbConnection = new PDO("mysql:host=$host;dbname=$nombrebd;port=$puerto", $usuario, $clave);    
                break;
                case "mongodb":
                    $dbConnection = new Mongodb("mongodb://$host"); 
                break;
            }
            if($tipo == "mongodb"){
                $dbConnection = $dbConnection->$nombrebd;
                return $dbConnection;
             }else{
                $dbConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                return $dbConnection; 
            }  
        };
        function coneccionMBD($id) {
            $data = \DB::select("select * from sp_obtener_servicio(?)",array($id));         
            $tipo = $data[0]->_ser_gestor;
            $nombrebd = $data[0]->_ser_bd;
            $host = $data[0]->_ser_host;
            $puerto = $data[0]->_ser_puerto;
            $clave = $data[0]->_ser_clave;
            $usuario = $data[0]->_ser_usuario;
            switch ($tipo){
                case "postgres":
                    $dbConnection = new PDO("pgsql:dbname=$nombrebd;host=$host;port=$puerto", $usuario, $clave);

                break;
                case "mssql":
                    $dbConnection = new PDO("dblib:host=$host;dbname=$nombrebd", $usuario, $clave);
                break;
                case "mysql":
                    $dbConnection = new PDO("mysql:host=$host;dbname=$nombrebd;port=$puerto", $usuario, $clave);

                break;
                case "mongodb":
                    $dbConnection = new Mongodb("mongodb://$host");
                break;
            }
            if($tipo == "mongodb"){
                $dbConnection = $dbConnection->$nombrebd;
                return $dbConnection;
             }else{
                $dbConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                return $dbConnection;
            }
        };
        function coneccionBD($id) {
            $data = \DB::select("select * from sp_obtener_servicio(?)",array($id));         
            $tipo = $data[0]->_ser_gestor;
            $nombrebd = $data[0]->_ser_bd;
            $host = $data[0]->_ser_host;
            $puerto = $data[0]->_ser_puerto;
            $clave = $data[0]->_ser_clave;
            $usuario = $data[0]->_ser_usuario;
            switch ($tipo){
                case "postgres":
                    $dbConnection = new PDO("pgsql:dbname=$nombrebd;host=$host;port=$puerto", $usuario, $clave);
                break;
                case "mssql":
                    $dbConnection = new PDO("dblib:host=$host;dbname=$nombrebd", $usuario, $clave);
                break;
                case "mysql":
                    $dbConnection = new PDO("mysql:host=$host;dbname=$nombrebd;port=$puerto", $usuario, $clave);

                break;
                case "mongodb":
                    $dbConnection = new Mongodb("mongodb://$host");
                break;
            }
            if($tipo == "mongodb"){
                $dbConnection = $dbConnection->$nombrebd;
                return $dbConnection;
             }else{
                $dbConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                return $dbConnection;
            }
        };
        function getDB2($base,$host,$usuario,$clave,$nombrebd,$puerto) {
            switch ($base){
                case "postgres":
                    $dbConnection = new PDO("pgsql:dbname=$nombrebd;host=$host;port=$puerto", $usuario, $clave);    
                break;
                case "mssql":
                    $dbConnection = new PDO("dblib:host=$host;dbname=$nombrebd", $usuario, $clave);
                break;
                case "mysql":
                    $dbConnection = new PDO("mysql:host=$host;dbname=$nombrebd;port=$puerto", $usuario, $clave);    
                break;
            }
            $dbConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            return $dbConnection;   
        }
        function ejecutar_Query($conn,$query,$tipo) {
            try {
                $array;$stmt;
                $stmt = $conn->prepare($query);
                switch ($tipo){
                    case "Select":
                    case "EXEC": 
                        $stmt->execute();
                        $rows = $stmt->fetchAll(PDO::FETCH_OBJ);
                        if(sizeof($rows) > 0) {$array = $rows;} else {$array = "[{ }]";}
                    break;
                    case "Insert":
                    case "Update":
                    case "Create":
                        if($stmt->execute()) {$array=true;} else{$array=false;}    
                    break;
                    case "Delet":
                        $stmt->execute();
                        if($stmt->rowCount()!=0) {$array=$stmt->rowCount();} else {$array=false;}
                    break;
                }
                return  json_encode($array);
                $stmt=null;
            } catch(PDOException $e) {
                $stmt=null; $error = array("error"=>$e->getMessage());
                return json_encode($error);
            }
        }
        function ejecutar_SP($id) {
            $data = \DB::select("select * from sp_obtener_servicio(?)",array($id));
            $gestor = $data[0]->_ser_gestor;
            $servicio = $data[0]->_ser_sp;
            $arg = json_decode($data[0]->_ser_argumentos,true);
            $paramEnvio="";
            if(count($arg)!=0)
            {
                $parametros = "";
                foreach($arg as $lin) {$parametros .= $lin.\',\';}
                $ln = strlen ($parametros);
                $paramEnvio = substr($parametros,0,$ln-1);
            }
            if($gestor!="mssql") {$sql = "SELECT * from $servicio($paramEnvio);";}
            else {$sql = "EXEC dbo.$servicio $paramEnvio;";}
            $db=null;$stmt=null;
            try {
                $db =getDB2($gestor,$data[0]->_ser_host,$data[0]->_ser_usuario,$data[0]->_ser_clave,$data[0]->_ser_bd,$data[0]->_ser_puerto);
                $stmt = $db->prepare($sql);                
                $stmt->execute();
                $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
                if(sizeof($rows) > 0) {$array = $rows;} else {$array[0] = array("error" => "No se pudo realizar el servicio",);}
                $db=null;$stmt=null;
                return  json_encode($array);
            } catch(PDOException $e) {
                $db=null;$stmt=null; $error = array("errorqq"=>$e->getMessage());
                return json_encode($error);
            }
        }
        function ejecutar_REST($id){
            $success = array("code"    => 200);
            $error   = array("message" => "error de instancia", "code" => 602);
            $respuesta="";
            $cliente;
            try {
                $success = array("code" => 200);
                $data    = \DB::select("select * from sp_get_servicio_id(?)",array($id));
                if ($data[0]->_ser_tipo_servicio =="R")
                {
                    $endponit=$data[0]->_ser_endpoint;
                    $verbo=$data[0]->_ser_endpoint_verbo;
                    $parametros=$data[0]->_ser_argumentos;
                    $argumento=json_decode($parametros, true);
                    $clienteServicio = new \Ixudra\Curl\CurlService();
                    switch ($verbo) {
                        case "POST":
                            $cliente = $clienteServicio->to($endponit)
                            ->withContentType("application/json")
                            ->withData($argumento)->asJson()->post();
                        break;
                        case "GET":
                            $cliente = $clienteServicio->to($endponit)
                            ->withContentType("application/json")
                            ->withData($argumento)->asJson()->get();
                        break;
                    }
                    $respuesta = new collection($cliente);
            }
            $clienteServicio=null;$cliente=null;
            return json_encode($respuesta);
            } catch (Exception $error) {
                $clienteServicio=null;$cliente=null; return response()->json(["error" => $error]);
            }
        }
        function ejecutar_DB($cadena,$sql,$user,$pass,$tipo) {
            $conn=$cadena;
            $query=$sql;
            $dbuser=$user;
            $dbpass=$pass;
            $conn;$stmt;
            try {
                $array;
                $conn = new PDO($conn,$dbuser,$dbpass);
                $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                $stmt = $conn->prepare($query);
                switch ($tipo){
                    case "Select":
                    case "EXEC": 
                        $stmt->execute();
                        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
                        if(sizeof($rows) > 0) {$array = $rows;} else {$array = "[{ }]";}
                    break;
                    case "Insert":
                    case "Update":
                    case "Create":
                        if($stmt->execute()) {$array=true;} else{$array=false;}    
                    break;
                    case "Delet":
                        $stmt->execute();
                        if($stmt->rowCount()!=0) {$array=$stmt->rowCount();} else {$array=false;}
                    break;
                }
                $conn=null;$stmt=null;
                return  json_encode($array);
            } catch(PDOException $e) {
                $conn=null;$stmt=null; $error = array("error"=>$e->getMessage());
                return json_encode($error);
            }
        }
        ';
        $despues = '$conn=null;';
        if($request["identificador"] != null){
            $datos =$request["identificador"];
            $data = \DB::select('select sp_get_codigo(?)',array($datos)) ;
            $lineas = json_decode($data[0]->sp_get_codigo, true);
        }elseif ($request["id"] != null) {
            $datos =$request["id"];
            $data = \DB::select('select sp_get_codigo_idrn(?)',array($datos)) ;
            $lineas = json_decode($data[0]->sp_get_codigo_idrn, true);
        }
        $recopilado = "";
        $nro = 0;
        foreach($lineas as $lin) {
            $recopilado .= $lin["scd_codigo"] . "\n";
        }
        try {
            $recopilado=str_replace("&039;", "'", $recopilado);
            $recopilado=str_replace("&040;", "\"", $recopilado);
            $recopilado=str_replace("&041;", "<", $recopilado);
            $recopilado=str_replace("&042;", ">", $recopilado);
            $recopilado=str_replace("&043;", "\\", $recopilado);
            $res = eval($previoLib . $previoPrevio . $previo . $recopilado . $despues);
        } catch (Throwable $t) {
            $res = $t;
        }
        return $res;
    }

    public function darBajaRegla_Negocio(Request $data) {
        $data = \DB::select('select * from sp_baja_regla_negocio(?)',array($data['idReglaNegocio'])) ;
        return json_encode($data,true);
    }

    function filterXSS($val) {
        $val = preg_replace('/([\x00-\x08][\x0b-\x0c][\x0e-\x20])/', '', $val);
        $search = 'abcdefghijklmnopqrstuvwxyz';
        $search .= 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $search .= '1234567890!@#$%^&*()';
        $search .= '~`";:?+/={}[]-_|\'\\';
        for ($i = 0; $i < strlen($search); $i++) {
            $val = preg_replace('/(&#[x|X]0{0,8}'.dechex(ord($search[$i])).';?)/i', $search[$i], $val); // with a ;
            $val = preg_replace('/(&#0{0,8}'.ord($search[$i]).';?)/', $search[$i], $val); // with a ;
        }
        $ra1 = Array('javascript', 'vbscript', 'expression', 'applet', 'meta', 'xml', 'blink', 'link', 'style', 'script', 'embed', 'object', 'iframe', 'frame', 'frameset', 'ilayer', 'layer', 'bgsound', 'title', 'base');
        $ra2 = Array('onabort', 'onactivate', 'onafterprint', 'onafterupdate', 'onbeforeactivate', 'onbeforecopy', 'onbeforecut', 'onbeforedeactivate', 'onbeforeeditfocus', 'onbeforepaste', 'onbeforeprint', 'onbeforeunload', 'onbeforeupdate', 'onblur', 'onbounce', 'oncellchange', 'onchange', 'onclick', 'oncontextmenu', 'oncontrolselect', 'oncopy', 'oncut', 'ondataavailable', 'ondatasetchanged', 'ondatasetcomplete', 'ondblclick', 'ondeactivate', 'ondrag', 'ondragend', 'ondragenter', 'ondragleave', 'ondragover', 'ondragstart', 'ondrop', 'onerror', 'onerrorupdate', 'onfilterchange', 'onfinish', 'onfocus', 'onfocusin', 'onfocusout', 'onhelp', 'onkeydown', 'onkeypress', 'onkeyup', 'onlayoutcomplete', 'onload', 'onlosecapture', 'onmousedown', 'onmouseenter', 'onmouseleave', 'onmousemove', 'onmouseout', 'onmouseover', 'onmouseup', 'onmousewheel', 'onmove', 'onmoveend', 'onmovestart', 'onpaste', 'onpropertychange', 'onreadystatechange', 'onreset', 'onresize', 'onresizeend', 'onresizestart', 'onrowenter', 'onrowexit', 'onrowsdelete', 'onrowsinserted', 'onscroll', 'onselect', 'onselectionchange', 'onselectstart', 'onstart', 'onstop', 'onsubmit', 'onunload');
        $ra = array_merge($ra1, $ra2);
        $found = true;
        while ($found == true) {
            $val_before = $val;
            for ($i = 0; $i < sizeof($ra); $i++) {
                $pattern = '/';
                for ($j = 0; $j < strlen($ra[$i]); $j++) {
                    if ($j > 0) {
                        $pattern .= '(';
                        $pattern .= '(&#[x|X]0{0,8}([9][a][b]);?)?';
                        $pattern .= '|(&#0{0,8}([9][10][13]);?)?';
                        $pattern .= ')?';
                }
                $pattern .= $ra[$i][$j];
             }
             $pattern .= '/i';
             $replacement = substr($ra[$i], 0, 2).'<x>'.substr($ra[$i], 2);
             $val = preg_replace($pattern, $replacement, $val);
             if ($val_before == $val) {
                $found = false;
             }
          }
        }
        return $val;
    }

}