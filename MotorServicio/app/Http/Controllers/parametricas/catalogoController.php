<?php

namespace gamlp\Http\Controllers\parametricas;

use Illuminate\Http\Request;
use gamlp\Http\Controllers\Controller;
use gamlp\Modelo\parametricas\Catalogo;
use Yajra\Datatables\Datatables;
use Auth;

class catalogoController extends Controller
{
    public function index()
    {
        $catalogo   = Catalogo::OrderBy('ctp_id', 'desc')->pluck('ctp_descripcion', 'ctp_id');
        return view('backend.administracion.parametricas.catalogo.index', compact('catalogo'));
    }

    public function create()
    {
        $catalogo = Catalogo::getListarCatalogo();
        return Datatables::of($catalogo)->addColumn('acciones', function ($catalogo) {
        return '<button value="' . $catalogo->ctp_id . '" class="btncirculo btn-xs btn-primary" style="background:#57BC90" 
                onClick="MostrarCatalogo(this);" data-toggle="modal" data-target="#myUpdateCatalogo"><i class="fa fa-pencil-square"></i></button>

            <button value="' . $catalogo->ctp_id . '" class="btncirculo btn-xs btn-warning" style="background:#7ACCCE" onClick="Eliminar(this);"><i class="fa fa-trash-o"></i></button>';           ;
             
        }) ->make(true);
    }

    public function store(Request $request)
    {
        $id = Catalogo::getId();
        $nid = $id['ctp_id'];
        $nid = $nid + 1;
        $ids = Auth::user()->usr_id;

        if ($request->ajax()) {
            Catalogo::create([
                'ctp_id'            => $nid,
                'ctp_descripcion'   => $request['ctp_descripcion'],
                'ctp_clasificador'  => $request['ctp_clasificador'],
                'ctp_codigo'  => $request['ctp_codigo'],
                'ctp_usr_id'       => $ids,
                'ctp_estado'        => 'A' ,
            ]);
            return response()->json(['Mensaje' => 'Opcion creado']);
        } else {
            return response()->json(['Mensaje' => 'Opcion no fue registrado']);
        }

        return response()->json();
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $catalogo = Catalogo::getCatalogo($id);
        return response()->json($catalogo); 
    }

    public function update(Request $request, $id)
    {
        $catalogo = Catalogo::getCatalogo($id);
        $catalogo -> fill($request->all());
        $catalogo -> save();
        return response() -> json($catalogo -> toArray());
    }

    public function destroy($id)
    {
        $catalogo= Catalogo::getDestroy($id);
        return response()->json($catalogo);
    }
}
