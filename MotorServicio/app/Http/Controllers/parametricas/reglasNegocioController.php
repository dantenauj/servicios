<?php

namespace gamlp\Http\Controllers\parametricas;

use Auth;
use gamlp\Http\Controllers\Controller;
use gamlp\Modelo\parametricas\Catalogo;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Ixudra\Curl\Facades\Curl;

use MongoDB\Client as Mongodb;
use PDO;
use Session;

use Yajra\Datatables\Datatables;

class reglasNegocioController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */

	public function index() {

	
		/*$host = "192.168.5.243";
		$nombrebd = "RC_Lapaz";
		$dbConnection = new Mongodb("mongodb://$host");

 		$dbConnection = $dbConnection->$nombrebd->ciudadanos;
		$ci          = "4409485";
		$js          = "function() {
    						return this.dtspsl_ci == '".$ci."';
					   }";
		$cursor1 = $dbConnection->findOne(array('$where' => $js));
		$cursor2 = JSON_encode($cursor1, true);
		$detalletotal = array();
		foreach ($cursor1 as $clave => $datos) {
			$dataFinal = $clave.':'.$datos;
			array_push($detalletotal, $dataFinal);
		}
		echo ("<pre>");
		var_dump($detalletotal);
		echo ("<pre>");*/

		//phpinfo();
		$par = Catalogo::OrderBy('ctp_id', 'asc')->pluck('ctp_descripcion', 'ctp_id');
		if (Auth::user()->usr_id != Session::get('ID_USUARIO')) {return view('auth.login');} else {
			return view('backend.administracion.parametricas.reglasNegocio.index',compact('par'));
		}
	}

	function get_conexionDB(Request $request) {
		$dbhost   = $request['host'];
		$dbuser   = $request['usuario'];
		$dbpass   = $request['clave'];
		$dbpasscr = $request['clave'];
		//Crypt::encrypt($request['clave']);
		$dbname   = $request['nombrebd'];
		$dbpuerto = $request['puerto'];
		$base     = $request['base'];
		$dbConnSTR;
		try {
			switch ($base) {
				case "mysql":
					$dbConnSTR = "mysql:host=$dbhost;dbname=$dbname;port=$dbpuerto";

					break;
				case "postgres":
					$dbConnSTR = "pgsql:dbname=$dbname;host=$dbhost;port=$dbpuerto";

					break;
				case "mssql":
					$dbConnSTR = "dblib:host=$dbhost;dbname=$dbname";
					break;
				case "mongodb":
					$dbConnSTR = "mongodb://$dbhost";
					break;
			}
			if ($base == "mongodb") {
				$conexión = new Mongodb($dbConnSTR);
				$dbConn    = $conexión->$dbname;
			} else {
				$dbConn = new PDO($dbConnSTR, $dbuser, $dbpass);
				$dbConn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			}

			$dbConn       = null;
			$dato         = array();
			$dato['res']  = $dbConnSTR;
			$dato['pass'] = $dbpasscr;
			return json_encode($dato);
		} catch (PDOException $e) {
			die("Sin conecxión: ".$e->getMessage());
			return json_encode(fale);
		}
	}

	public function get_restDinamico_CURL2($metodo, $url, $parametros) {
		$argumento       = json_decode($parametros, true);
		$clienteServicio = new \Ixudra\Curl\CurlService();
		switch ($metodo) {
			case 'POST':
				$cliente = $clienteServicio->to($url)
				                           ->withContentType('application/json')
				                           ->withData($argumento)->asJson()->post();
				break;
			case 'GET':
				$cliente = $clienteServicio->to($url)
				                           ->withContentType('application/json')
				                           ->withData($argumento)->asJson()->get();
				break;
			case '':

				break;
		}

		$resultado = new collection($cliente);
		return $resultado;
		//Datatables::of($resultado)->make(true);
	}

	public function lst_reglasNegocio(Request $data) {
		//echo Auth::user()->usr_id.'<>'.Session::get('ID_USUARIO');
		if (Auth::user()->usr_id != Session::get('ID_USUARIO')) {return view('frontend.login');} else {
			$data  = \DB::select('select * from sp_lst_codigo2()');
			$data2 = new Collection($data);
			return Datatables::of($data2)
				->editColumn('acciones', '<button class="btncirculo" style="background:#29b6f6" fa fa-plus-square pull-right" data-target="#modalUpdate" data-toggle="modal" data-placement="top" onClick="lst_codigo({{$_scd_id}})" title="Modificar" type="button"><i class="glyphicon glyphicon-pencil"></i></button> <button class="btncirculo" style="background:#ef5350" onClick="darBajaRegladeNegocio({{$_scd_id}});" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="glyphicon glyphicon-trash"></i></button>')
				->addColumn('_scd_codigo', function ($data2) {
					$recopilado = substr($data2->_scd_data, 17, 10).'  ...';
					/*foreach(json_decode($data2->_scd_data) as $lin) {
					$recopilado .= $lin->scd_codigo;
					}*/
					return $recopilado;
					;

				})
				->make(true);
		}

	}

	public function lst_reglasNegocio1(Request $data) {
		$value= $data['Workspace'];
		//dd($value);
		//echo Auth::user()->usr_id.'<>'.Session::get('ID_USUARIO');
		if (Auth::user()->usr_id != Session::get('ID_USUARIO')) {return view('frontend.login');} else {
			$data  = \DB::select('select * from sp_lst_codigo_rn(?)', array($value));
			//dd($data);
			$data2 = new Collection($data);
			//dd($data2);
			return response()->json(["Mensaje" => $data2]);
			/*return Datatables::of($data2)
				->editColumn('acciones', '<button class="btncirculo" style="background:#29b6f6" fa fa-plus-square pull-right" data-target="#modalUpdate" data-toggle="modal" data-placement="top" onClick="lst_codigo({{$_scd_id}})" title="Modificar" type="button"><i class="glyphicon glyphicon-pencil"></i></button> <button class="btncirculo" style="background:#ef5350" onClick="darBajaRegladeNegocio({{$_scd_id}});" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="glyphicon glyphicon-trash"></i></button>')
				->addColumn('_scd_codigo', function ($data2) {
					$recopilado = substr($data2->_scd_data, 17, 10).'  ...';
					return $recopilado;
					;

				})
				->make(true);*/
		}

	}

	public function lst_reglasNegocioCombo(Request $data) {
		$data = \DB::select('select * from sp_lst_regla_nombre()');
		return $data;
	}

	public function lst_reglasServicioCombo($tipo) {
		$data = \DB::select('select * from sp_get_servico_combo(?)', array($tipo));
		return $data;
	}

	public function ejecutarQuery(Request $request) {
		$data     = \DB::select("select * from sp_obtener_servicio(?)", array($request["id"]));
		$gestor   = $data[0]->_ser_gestor;
		$servicio = $data[0]->_ser_sp;
		$arg      = json_decode($data[0]->_ser_argumentos, true);

		$paramEnvio = "";
		if (count($arg) != 0) {
			$parametros = "";
			foreach ($arg as $lin) {
				$parametros .= $lin.",";
			}
			$ln         = strlen($parametros);
			$paramEnvio = substr($parametros, 0, $ln-1);
		}
		if ($gestor != 'mssql') {$sql = "SELECT * from $servicio($paramEnvio);";} else { $sql = "EXEC dbo.$servicio $paramEnvio;";}
		$db;
		$stmt;
		try {
			$db   = self::getDB($gestor, $data[0]->_ser_host, $data[0]->_ser_usuario, $data[0]->_ser_clave, $data[0]->_ser_bd, $data[0]->_ser_puerto);
			$stmt = $db->prepare($sql);
			$stmt->execute();
			$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
			$dato = 0;
			$array;
			if (sizeof($rows) > 0) {
				$array = $rows;
			} else {
				$array[0] = array(
					"error" => "No se pudo realizar el servicio",
				);
			}
			$db   = null;
			$stmt = null;
			return $array;
		} catch (PDOException $e) {
			$db    = null;
			$stmt  = null;
			$error = array("error" => $e->getMessage());
			return $error;
		}
	}

	function getDB($base, $host, $usuario, $clave, $nombrebd, $puerto) {
		switch ($base) {
			case "mysql":
				$dbConnection = new PDO("mysql:host=$host;dbname=$nombrebd;port=$puerto", $usuario, $clave);

				break;
			case "postgres":
				$dbConnection = new PDO("pgsql:dbname=$nombrebd;host=$host;port=$puerto", $usuario, $clave);

				break;
			case "mssql":
				$dbConnection = new PDO("dblib:host=$host;dbname=$nombrebd", $usuario, $clave);
				break;
		}
		$dbConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		return $dbConnection;
	}

	public function get_reglanegocioID($id) {
		$data = \DB::select('select * from sp_get_codigo_id(?)', array($id));
		return $data;
	}

	public function editarCodigo(Request $datacod) {
		$data = \DB::select('select * from sp_modifica_codigo(?,?,?,?)', array(
				$datacod['id'], $datacod['datosCodigo'], $datacod['propiedades'], $datacod['identificador']));
		return $data;
	}

	public function editarDataCodigo(Request $datacod) {
		$data = \DB::select('select * from sp_update_datocodigo(?,?)', array($datacod['id'], $datacod['datosCodigo']));
		return $data;
	}

	public function get_restCodigo($id) {
		$success   = array("code"    => 200);
		$error     = array("message" => "error de instancia", "code" => 602);
		$respuesta = '';
		try {
			$success = array("code" => 200);
			$data    = \DB::select('select * from s_codigo where scd_id =?', array($id));
			$lineas  = json_decode($data[0]->scd_data, true);
			return $lineas;
		} catch (Exception $error) {
			return response()->json(["error" => $error]);
		}
	}

	public function insertarRegla(Request $request) {
		$data = \DB::select('select sp_insert_regla(?,?,?,?,?)', array($request['datos'], $request['propiedades'], $request['usuario'],$request['workspace'],$request['identificador']));
		return $data;
	}

	function ejecutarConexionDB(Request $request) {
		$data   = \DB::select("select * from sp_obtener_servicio(?)", array($request["id"]));
		$base   = $data[0]->_ser_gestor;
		$dbhost = $data[0]->_ser_host;
		$dbuser = $data[0]->_ser_usuario;
		$dbpass = $data[0]->_ser_clave;
		//Crypt::decrypt($data[0]->_ser_clave);
		$dbname    = $data[0]->_ser_bd;
		$dbpuerto  = $data[0]->_ser_puerto;
		$dbConnSTR = $data[0]->_ser_sp;

		try {
			switch ($base) {
				case "mysql":
					$dbConnSTR = "mysql:host=$dbhost;dbname=$dbname;port=$dbpuerto";

					break;
				case "postgres":
					$dbConnSTR = "pgsql:dbname=$dbname;host=$dbhost;port=$dbpuerto";

					break;
				case "mssql":
					$dbConnSTR = "dblib:host=$dbhost;dbname=$dbname";
					break;
				case "mongodb":
					$dbConnSTR = "mongodb://$dbhost";
					break;
			}
			$dato = array();
			if ($base == "mongodb") {
				$conexión      = new Mongodb($dbConnSTR);
				$dbConn         = $conexión->$dbname;
				$dato['res']    = $dbConnSTR;
				$dato['tipo']   = $base;
				$dato['bd']     = $dbname;
				$dato['puerto'] = $dbpuerto;
				$dato['host']   = $dbhost;
			} else {
				$dbConn = new PDO($dbConnSTR, $dbuser, $dbpass);
				$dbConn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$dato['res']         = $dbConnSTR;
				$dato['tipo']        = $base;
				$dato['bd']          = $dbname;
				$dato['puerto']      = $dbpuerto;
				$dato['host']        = $dbhost;
				$dato['usuario']     = $dbuser;
				$dato['contraseña'] = $dbpass;
			}

			$dbConn = null;
			return json_encode($dato);
		} catch (PDOException $e) {
			die("Sin conecxión: ".$e->getMessage());
			return json_encode(fale);
		}
	}

	public function ejecutar_unitario(Request $request) {		
		$previo ='
        use Illuminate\Support\Collection;
        use MongoDB\Client as Mongodb;
        function ejecutar_Conn($id) {
            $data = \DB::select("select * from sp_obtener_servicio(?)",array($id));
            try {
                $dbConn =getDB2($data[0]->_ser_gestor,$data[0]->_ser_host,$data[0]->_ser_usuario,$data[0]->_ser_clave,$data[0]->_ser_bd,$data[0]->_ser_puerto);
                return $dbConn;
            } catch (PDOException $e) {
                            die("Sin conecxión: " . $e->getMessage());
                            return json_encode(fale);
            }
        };
        function coneccionMDB($tipo,$nombrebd,$host,$puerto) {
            switch ($tipo){
                case "postgres":
                    $dbConnection = new PDO("pgsql:dbname=$nombrebd;host=$host;port=$puerto", $usuario, $clave);

                break;
                case "mssql":
                    $dbConnection = new PDO("dblib:host=$host;dbname=$nombrebd", $usuario, $clave);
                break;
                case "mysql":
                    $dbConnection = new PDO("mysql:host=$host;dbname=$nombrebd;port=$puerto", $usuario, $clave);

                break;
                case "mongodb":
                    $dbConnection = new Mongodb("mongodb://$host");
                break;
            }
            if($tipo == "mongodb"){
                $dbConnection = $dbConnection->$nombrebd;
                return $dbConnection;
             }else{
                $dbConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                return $dbConnection;
            }
        };
        function coneccionMBD($id) {
            $data = \DB::select("select * from sp_obtener_servicio(?)",array($id));        	
        	$tipo = $data[0]->_ser_gestor;
	        $nombrebd = $data[0]->_ser_bd;
	        $host = $data[0]->_ser_host;
	        $puerto = $data[0]->_ser_puerto;
	        $clave = $data[0]->_ser_clave;
	        $usuario = $data[0]->_ser_usuario;
            switch ($tipo){
                case "postgres":
                    $dbConnection = new PDO("pgsql:dbname=$nombrebd;host=$host;port=$puerto", $usuario, $clave);

                break;
                case "mssql":
                    $dbConnection = new PDO("dblib:host=$host;dbname=$nombrebd", $usuario, $clave);
                break;
                case "mysql":
                    $dbConnection = new PDO("mysql:host=$host;dbname=$nombrebd;port=$puerto", $usuario, $clave);

                break;
                case "mongodb":
                    $dbConnection = new Mongodb("mongodb://$host");
                break;
            }
            if($tipo == "mongodb"){
                $dbConnection = $dbConnection->$nombrebd;
                return $dbConnection;
             }else{
                $dbConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                return $dbConnection;
            }
        };
        function coneccionDB($tipo,$nombrebd,$host,$puerto,$usuario,$clave) {
            switch ($tipo){
                case "postgres":
                    $dbConnection = new PDO("pgsql:dbname=$nombrebd;host=$host;port=$puerto", $usuario, $clave);

                break;
                case "mssql":
                    $dbConnection = new PDO("dblib:host=$host;dbname=$nombrebd", $usuario, $clave);
                break;
                case "mysql":
                    $dbConnection = new PDO("mysql:host=$host;dbname=$nombrebd;port=$puerto", $usuario, $clave);

                break;
                case "mongodb":
                    $dbConnection = new Mongodb("mongodb://$host");
                break;
            }
            if($tipo == "mongodb"){
                $dbConnection = $dbConnection->$nombrebd;
                return $dbConnection;
             }else{
                $dbConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                return $dbConnection;
            }
        };
        function coneccionBD($id) {
            $data = \DB::select("select * from sp_obtener_servicio(?)",array($id));        	
        	$tipo = $data[0]->_ser_gestor;
	        $nombrebd = $data[0]->_ser_bd;
	        $host = $data[0]->_ser_host;
	        $puerto = $data[0]->_ser_puerto;
	        $clave = $data[0]->_ser_clave;
	        $usuario = $data[0]->_ser_usuario;
            switch ($tipo){
                case "postgres":
                    $dbConnection = new PDO("pgsql:dbname=$nombrebd;host=$host;port=$puerto", $usuario, $clave);

                break;
                case "mssql":
                    $dbConnection = new PDO("dblib:host=$host;dbname=$nombrebd", $usuario, $clave);
                break;
                case "mysql":
                    $dbConnection = new PDO("mysql:host=$host;dbname=$nombrebd;port=$puerto", $usuario, $clave);

                break;
                case "mongodb":
                    $dbConnection = new Mongodb("mongodb://$host");
                break;
            }
            if($tipo == "mongodb"){
                $dbConnection = $dbConnection->$nombrebd;
                return $dbConnection;
             }else{
                $dbConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                return $dbConnection;
            }
        };
        function getDB2($base,$host,$usuario,$clave,$nombrebd,$puerto) {
            switch ($base){
                case "postgres":
                    $dbConnection = new PDO("pgsql:dbname=$nombrebd;host=$host;port=$puerto", $usuario, $clave);

                break;
                case "mssql":
                    $dbConnection = new PDO("dblib:host=$host;dbname=$nombrebd", $usuario, $clave);
                break;
                case "mysql":
                    $dbConnection = new PDO("mysql:host=$host;dbname=$nombrebd;port=$puerto", $usuario, $clave);

                break;
            }
            $dbConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            return $dbConnection;
        }
        function ejecutar_Query($conn,$query,$tipo) {
            try {
                $array;
				$stmt;
                $stmt = $conn->prepare($query);
                switch ($tipo){
                    case "Select":
                    case "EXEC":
                        $stmt->execute();
                        $rows = $stmt->fetchAll(PDO::FETCH_OBJ);
                        if(sizeof($rows) > 0) {$array = $rows;} else {$array = "[{ }]";}
                    break;
                    case "Insert":
                    case "Update":
                    case "Create":
                        if($stmt->execute()) {$array=true;} else{$array=false;}
                    break;
                    case "Delet":
                        $stmt->execute();
                        if($stmt->rowCount()!=0) {$array=$stmt->rowCount();} else {$array=false;}
                    break;
                }
                return  json_encode($array);
                $stmt=null;
            } catch(PDOException $e) {
                $stmt=null;
 				$error = array("error"=>$e->getMessage());
                return json_encode($error);
            }
        }
        function ejecutar_SP($id) {
            $data = \DB::select("select * from sp_obtener_servicio(?)",array($id));
            $gestor = $data[0]->_ser_gestor;
            $servicio = $data[0]->_ser_sp;
            $arg = json_decode($data[0]->_ser_argumentos,true);
            $paramEnvio="";
            if(count($arg)!=0)
            {
                $parametros = "";
                foreach($arg as $lin) {$parametros .= $lin.\',\';}
                $ln = strlen ($parametros);
                $paramEnvio = substr($parametros,0,$ln-1);
            }
            if($gestor!="mssql") {$sql = "SELECT * from $servicio($paramEnvio);";}
            else {$sql = "EXEC dbo.$servicio $paramEnvio;";}
            $db=null;
			$stmt=null;
            try {
                $db =getDB2($gestor,$data[0]->_ser_host,$data[0]->_ser_usuario,$data[0]->_ser_clave,$data[0]->_ser_bd,$data[0]->_ser_puerto);
                $stmt = $db->prepare($sql);
                $stmt->execute();
                $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
                if(sizeof($rows) > 0) {$array = $rows;} else {$array[0] = array("error" => "No se pudo realizar el servicio",);}
                $db=null;
				$stmt=null;
                return  json_encode($array);
            } catch(PDOException $e) {
                $db=null;
			$stmt=null;
 			$error = array("errorqq"=>$e->getMessage());
                return json_encode($error);
            }
        }
        function ejecutar_REST($id){
            $success = array("code"    => 200);
            $error   = array("message" => "error de instancia", "code" => 602);
            $respuesta="";
            $cliente;
            try {
                $success = array("code" => 200);
                $data    = \DB::select("select * from sp_get_servicio_id(?)",array($id));
                if ($data[0]->_ser_tipo_servicio =="R")
                {
                    $endponit=$data[0]->_ser_endpoint;
                    $verbo=$data[0]->_ser_endpoint_verbo;
                    $parametros=$data[0]->_ser_argumentos;
                    $argumento=json_decode($parametros, true);
                    $clienteServicio = new \Ixudra\Curl\CurlService();
                    switch ($verbo) {
                        case "POST":
                            $cliente = $clienteServicio->to($endponit)
                            ->withContentType("application/json")
                            ->withData($argumento)->asJson()->post();
                        break;
                        case "GET":
                            $cliente = $clienteServicio->to($endponit)
                            ->withContentType("application/json")
                            ->withData($argumento)->asJson()->get();
                        break;
                    }
                    $respuesta = new collection($cliente);
            }
            $clienteServicio=null;
			$cliente=null;
            return json_encode($respuesta);
            } catch (Exception $error) {
                $clienteServicio=null;
				$cliente=null;
 				return response()->json(["error" => $error]);
            }
        }
        function ejecutar_DB($cadena,$sql,$user,$pass,$tipo) {
            $conn=$cadena;
            $query=$sql;
            $dbuser=$user;
            $dbpass=$pass;
            $conn;
			$stmt;
            try {
                $array;
                $conn = new PDO($conn,$dbuser,$dbpass);
                $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                $stmt = $conn->prepare($query);
                switch ($tipo){
                    case "Select":
                    case "EXEC":
                        $stmt->execute();
                        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
                        if(sizeof($rows) > 0) {$array = $rows;} else {$array = "[{ }]";}
                    break;
                    case "Insert":
                    case "Update":
                    case "Create":
                        if($stmt->execute()) {$array=true;} else{$array=false;}
                    break;
                    case "Delet":
                        $stmt->execute();
                        if($stmt->rowCount()!=0) {$array=$stmt->rowCount();} else {$array=false;}
                    break;
                }
                $conn=null;
				$stmt=null;
                return  json_encode($array);
            } catch(PDOException $e) {
                $conn=null;
				$stmt=null;
 				$error = array("error"=>$e->getMessage());
                return json_encode($error);
            }
        }
        ';
		$despues = '$conn=null;';
		$data    = $request['ejecutar'];
		try {
			$data = str_replace("&039;", "'", $data);
			$data = str_replace("&040;", "\"", $data);
			$data = str_replace("&041;", "<", $data);
			$data = str_replace("&042;", ">", $data);
			$data = str_replace("&043;", "\\", $data);
			$res  = eval($previo.$data.$despues);
			return $res;
			//memory_get_usage();
		} catch (Throwable $t) {
			return $t;
		}
	}

	public function ejecutar($id) {
		$previo = ''.
		'use Illuminate\Support\Collection;
        static $conn=array();
        function ejecutar_REST($id){
            $success = array("code"    => 200);
            $error   = array("message" => "error de instancia", "code" => 602);
            $respuesta="";
            $cliente;
            try {
                $success = array("code" => 200);
                $data    = \DB::select("select * from sp_get_servicio_id(?)",array($id));
                if ($data[0]->_ser_tipo_servicio =="R")
                {
                    $endponit=$data[0]->_ser_endpoint;
                    $verbo=$data[0]->_ser_endpoint_verbo;
                    $parametros=$data[0]->_ser_argumentos;
                    $argumento=json_decode($parametros, true);
                    $clienteServicio = new \Ixudra\Curl\CurlService();
                    switch ($verbo) {
                        case "POST":
                            $cliente = $clienteServicio->to($endponit)
                            ->withContentType("application/json")
                            ->withData($argumento)->asJson()->post();
                        break;
                        case "GET":
                            $cliente = $clienteServicio->to($endponit)
                            ->withContentType("application/json")
                            ->withData($argumento)->asJson()->get();
                        break;
                    }
                    $respuesta = new collection($cliente);
            }
            $clienteServicio=null;
			$cliente=null;
            return json_encode($respuesta);
            } catch (Exception $error) {
                $clienteServicio=null;
				$cliente=null;
 				return response()->json(["error" => $error]);
            }}; '.
		'function ejecutar_SP($id) {
            $data = \DB::select("select * from sp_obtener_servicio(?)",array($id));
            $gestor = $data[0]->_ser_gestor;
            $servicio = $data[0]->_ser_sp;
            $arg = json_decode($data[0]->_ser_argumentos,true);
            $paramEnvio="";
            if(count($arg)!=0)
            {
                $parametros = "";
                foreach($arg as $lin) {$parametros .= $lin.\',\';}
                $ln = strlen ($parametros);
                $paramEnvio = substr($parametros,0,$ln-1);
            }
            if($gestor!="mssql") {$sql = "SELECT * from $servicio($paramEnvio);";}
            else {$sql = "EXEC dbo.$servicio $paramEnvio;";}
            $db=null;
			$stmt=null;
            try {
                $db =getDB2($gestor,$data[0]->_ser_host,$data[0]->_ser_usuario,$data[0]->_ser_clave,$data[0]->_ser_bd,$data[0]->_ser_puerto);
                $stmt = $db->prepare($sql);
                $stmt->execute();
                $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
                if(sizeof($rows) > 0) {$array = $rows;} else {$array[0] = array("error" => "No se pudo realizar el servicio",);}
                $db=null;
				$stmt=null;
                return  json_encode($array);
            } catch(PDOException $e) {
                $db=null;
				$stmt=null;
 				$error = array("errorqq"=>$e->getMessage());
                return json_encode($error);
            }
        };
        function coneccionMDB($tipo,$nombrebd,$host,$puerto) {
            switch ($tipo){
                case "postgres":
                    $dbConnection = new PDO("pgsql:dbname=$nombrebd;host=$host;port=$puerto", $usuario, $clave);

                break;
                case "mssql":
                    $dbConnection = new PDO("dblib:host=$host;dbname=$nombrebd", $usuario, $clave);
                break;
                case "mysql":
                    $dbConnection = new PDO("mysql:host=$host;dbname=$nombrebd;port=$puerto", $usuario, $clave);

                break;
                case "mongodb":
                    $dbConnection = new Mongodb("mongodb://$host");
                break;
            }
            if($tipo == "mongodb"){
                $dbConnection = $dbConnection->$nombrebd;
                return $dbConnection;
             }else{
                $dbConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                return $dbConnection;
            }
        };
        function coneccionDB($tipo,$nombrebd,$host,$puerto,$usuario,$clave) {
            switch ($tipo){
                case "postgres":
                    $dbConnection = new PDO("pgsql:dbname=$nombrebd;host=$host;port=$puerto", $usuario, $clave);

                break;
                case "mssql":
                    $dbConnection = new PDO("dblib:host=$host;dbname=$nombrebd", $usuario, $clave);
                break;
                case "mysql":
                    $dbConnection = new PDO("mysql:host=$host;dbname=$nombrebd;port=$puerto", $usuario, $clave);

                break;
                case "mongodb":
                    $dbConnection = new Mongodb("mongodb://$host");
                break;
            }
            if($tipo == "mongodb"){
                $dbConnection = $dbConnection->$nombrebd;
                return $dbConnection;
             }else{
                $dbConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                return $dbConnection;
            }
        };
        function coneccionMBD($id) {
            $data = \DB::select("select * from sp_obtener_servicio(?)",array($id));        	
        	$tipo = $data[0]->_ser_gestor;
	        $nombrebd = $data[0]->_ser_bd;
	        $host = $data[0]->_ser_host;
	        $puerto = $data[0]->_ser_puerto;
	        $clave = $data[0]->_ser_clave;
	        $usuario = $data[0]->_ser_usuario;
            switch ($tipo){
                case "postgres":
                    $dbConnection = new PDO("pgsql:dbname=$nombrebd;host=$host;port=$puerto", $usuario, $clave);

                break;
                case "mssql":
                    $dbConnection = new PDO("dblib:host=$host;dbname=$nombrebd", $usuario, $clave);
                break;
                case "mysql":
                    $dbConnection = new PDO("mysql:host=$host;dbname=$nombrebd;port=$puerto", $usuario, $clave);

                break;
                case "mongodb":
                    $dbConnection = new Mongodb("mongodb://$host");
                break;
            }
            if($tipo == "mongodb"){
                $dbConnection = $dbConnection->$nombrebd;
                return $dbConnection;
             }else{
                $dbConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                return $dbConnection;
            }
        };
		function coneccionBD($id) {
            $data = \DB::select("select * from sp_obtener_servicio(?)",array($id));        	
        	$tipo = $data[0]->_ser_gestor;
	        $nombrebd = $data[0]->_ser_bd;
	        $host = $data[0]->_ser_host;
	        $puerto = $data[0]->_ser_puerto;
	        $clave = $data[0]->_ser_clave;
	        $usuario = $data[0]->_ser_usuario;
            switch ($tipo){
                case "postgres":
                    $dbConnection = new PDO("pgsql:dbname=$nombrebd;host=$host;port=$puerto", $usuario, $clave);
                break;
                case "mssql":
                    $dbConnection = new PDO("dblib:host=$host;dbname=$nombrebd", $usuario, $clave);
                break;
                case "mysql":
                    $dbConnection = new PDO("mysql:host=$host;dbname=$nombrebd;port=$puerto", $usuario, $clave);

                break;
                case "mongodb":
                    $dbConnection = new Mongodb("mongodb://$host");
                break;
            }
            if($tipo == "mongodb"){
                $dbConnection = $dbConnection->$nombrebd;
                return $dbConnection;
             }else{
                $dbConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                return $dbConnection;
            }
        };
        function getDB2($base,$host,$usuario,$clave,$nombrebd,$puerto) {
           $dbConnection;
            switch ($base){
                case "mysql":
                    $dbConnection = new PDO("mysql:host=$host;dbname=$nombrebd;port=$puerto", $usuario, $clave);

                break;
                case "postgres":
                    $dbConnection = new PDO("pgsql:dbname=$nombrebd;host=$host;port=$puerto", $usuario, $clave);

                break;
                case "mssql":
                    $dbConnection = new PDO("dblib:host=$host;dbname=$nombrebd", $usuario, $clave);
                break;
            }
            $dbConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            return $dbConnection;
        };'.
		'function ejecutar_DB($cadena,$sql,$user,$pass,$tipo) {
            $conn=$cadena;
            $query=$sql;
            $dbuser=$user;
            $dbpass=$pass;
            $query=$sql;
            $conn;
			$stmt;
            try {
                $array;
                $conn = new PDO($conn,$dbuser,$dbpass);
                $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                $stmt = $conn->prepare($query);
                switch ($tipo){
                    case "Select":
                    case "EXEC":
                        $stmt->execute();
                        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
                        if(sizeof($rows) > 0) {$array = $rows;} else {$array = "[{ }]";}
                    break;
                    case "Insert":
                    case "Update":
                    case "Create":
                        if($stmt->execute()) {$array=true;} else{$array=false;}
                    break;
                    case "Delet":
                        $stmt->execute();
                        if($stmt->rowCount()!=0) {$array=$stmt->rowCount();} else {$array=false;}
                    break;
                }
                $conn=null;
				$stmt=null;
                return  json_encode($array);
            } catch(PDOException $e) {
                $conn=null;
				$stmt=null;
 				$error = array("error"=>$e->getMessage());
                return json_encode($error);
            }
        };
        function ejecutar_Conn($id) {
            $data = \DB::select("select * from sp_obtener_servicio(?)",array($id));
            $dbConnSTR = $data[0]->_ser_sp;
            try {
                $dbConn =getDB2($data[0]->_ser_gestor,$data[0]->_ser_host,$data[0]->_ser_usuario,$data[0]->_ser_clave,$data[0]->_ser_bd,$data[0]->_ser_puerto);
                return $dbConn;

            } catch (PDOException $e) {
                            die("Sin conecxión: " . $e->getMessage());
                            return json_encode(fale);
            }
        }
        function ejecutar_Query($conn,$query,$tipo) {
            try {
                $array;
				$stmt;
                $stmt = $conn->prepare($query);
                switch ($tipo){
                    case "Select":
                    case "EXEC":
                        $stmt->execute();
                        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
                        if(sizeof($rows) > 0) {$array = $rows;} else {$array = "[{ }]";}
                    break;
                    case "Insert":
                    case "Update":
                    case "Create":
                        if($stmt->execute()) {$array=true;} else{$array=false;}
                    break;
                    case "Delet":
                        $stmt->execute();
                        if($stmt->rowCount()!=0) {$array=$stmt->rowCount();} else {$array=false;}
                    break;
                }
                $stmt=null;
                return  json_encode($array);
            } catch(PDOException $e) {
                $stmt=null;
 				$error = array("error"=>$e->getMessage());
                return json_encode($error);
            }
        };';
		$despues = '$conn=null;';
		//'foreach($conn as $posicion => $connx) {$conn[$posicion]=null;}';
		$data       = \DB::select('select sp_get_codigo(?)', array($id));
		$lineas     = json_decode($data[0]->sp_get_codigo, true);
		$recopilado = "";
		$nro        = 0;
		foreach ($lineas as $lin) {
			$recopilado .= $lin["scd_codigo"];
		}
		try {
			//$recopilado=strip_tags($recopilado);
			$recopilado = str_replace("&039;", "'", $recopilado);
			$recopilado = str_replace("&040;", "\"", $recopilado);
			$recopilado = str_replace("&041;", "<", $recopilado);
			$recopilado = str_replace("&042;", ">", $recopilado);
			$recopilado = str_replace("&043;", "\\", $recopilado);
			$res        = eval($previo.$recopilado.$despues);
		} catch (Throwable $t) {
			$res = $t;
		}
		return $res;
	}

	public function ejecutarREST(Request $request) {
		$success   = array("code"    => 200);
		$error     = array("message" => "error de instancia", "code" => 602);
		$respuesta = '';
		try {
			$success = array("code" => 200);
			$data    = \DB::select('select * from sp_get_servicio_id(?)', array($request['id']));
			if ($data[0]->_ser_tipo_servicio == 'R') {
				$endponit   = $data[0]->_ser_endpoint;
				$verbo      = $data[0]->_ser_endpoint_verbo;
				$argumentos = $data[0]->_ser_argumentos;
				$respuesta  = reglasNegocioController::get_restDinamico_CURL2($verbo, $endponit, $argumentos);
			}
			return $respuesta;
			//response()->json(["data" => $respuesta]);
		} catch (Exception $error) {
			dd('->', $error);
			return response()->json(["error" => $error]);

		}
	}

	public function darBajaRegla_Negocio(Request $data) {
		$data = \DB::select('select * from sp_baja_regla_negocio(?)', array($data['idReglaNegocio']));
		return json_encode($data, true);
	}

	function filterXSS($val) {
		$val    = preg_replace('/([\x00-\x08][\x0b-\x0c][\x0e-\x20])/', '', $val);
		$search = 'abcdefghijklmnopqrstuvwxyz';
		$search .= 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$search .= '1234567890!@#$%^&*()';
		$search .= '~`";:?+/={}[]-_|\'\\';
		for ($i = 0; $i < strlen($search); $i++) {
			$val = preg_replace('/(&#[x|X]0{0,8}'.dechex(ord($search[$i])).';?)/i', $search[$i], $val);
			// with a ;
			$val = preg_replace('/(&#0{0,8}'.ord($search[$i]).';?)/', $search[$i], $val);
			// with a ;
		}
		$ra1 = Array('javascript', 'vbscript', 'expression', 'applet', 'meta', 'xml', 'blink', 'link', 'style', 'script', 'embed', 'object', 'iframe', 'frame', 'frameset', 'ilayer', 'layer', 'bgsound', 'title', 'base');
		$ra2 = Array('onabort', 'onactivate', 'onafterprint', 'onafterupdate', 'onbeforeactivate', 'onbeforecopy', 'onbeforecut', 'onbeforedeactivate', 'onbeforeeditfocus', 'onbeforepaste', 'onbeforeprint', 'onbeforeunload', 'onbeforeupdate', 'onblur', 'onbounce', 'oncellchange', 'onchange', 'onclick', 'oncontextmenu', 'oncontrolselect', 'oncopy', 'oncut', 'ondataavailable', 'ondatasetchanged', 'ondatasetcomplete', 'ondblclick', 'ondeactivate', 'ondrag', 'ondragend', 'ondragenter', 'ondragleave', 'ondragover', 'ondragstart', 'ondrop', 'onerror', 'onerrorupdate', 'onfilterchange', 'onfinish', 'onfocus', 'onfocusin', 'onfocusout', 'onhelp', 'onkeydown', 'onkeypress', 'onkeyup', 'onlayoutcomplete', 'onload', 'onlosecapture', 'onmousedown', 'onmouseenter', 'onmouseleave', 'onmousemove', 'onmouseout', 'onmouseover', 'onmouseup', 'onmousewheel', 'onmove', 'onmoveend', 'onmovestart', 'onpaste', 'onpropertychange', 'onreadystatechange', 'onreset', 'onresize', 'onresizeend', 'onresizestart', 'onrowenter', 'onrowexit', 'onrowsdelete', 'onrowsinserted', 'onscroll', 'onselect', 'onselectionchange', 'onselectstart', 'onstart', 'onstop', 'onsubmit', 'onunload');
		$ra  = array_merge($ra1, $ra2);

		$found = true;
		while ($found == true) {
			$val_before = $val;
			for ($i = 0; $i < sizeof($ra); $i++) {
				$pattern = '/';
				for ($j = 0; $j < strlen($ra[$i]); $j++) {
					if ($j > 0) {
						$pattern .= '(';
						$pattern .= '(&#[x|X]0{0,8}([9][a][b]);?)?';
						$pattern .= '|(&#0{0,8}([9][10][13]);?)?';
						$pattern .= ')?';
					}
					$pattern .= $ra[$i][$j];
				}
				$pattern .= '/i';
				$replacement = substr($ra[$i], 0, 2).'<x>'.substr($ra[$i], 2);
				$val         = preg_replace($pattern, $replacement, $val);
				if ($val_before == $val) {
					$found = false;
				}
			}
		}
		return $val;
	}

	public function identificador(Request $data) {

		$data = \DB::select('select * from sp_identificador(?)', array($data['idworkspace']));
		return json_encode($data, true);
	}
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		//
	}
}
