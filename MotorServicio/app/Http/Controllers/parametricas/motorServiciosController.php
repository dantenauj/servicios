<?php

namespace gamlp\Http\Controllers\parametricas;

use Auth;
use gamlp\Http\Controllers\Controller;
use gamlp\Modelo\parametricas\Catalogo;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Ixudra\Curl\Facades\Curl;
use MongoDB\Client as Mongodb;
use PDO;
use Session;

use Yajra\Datatables\Datatables;

class motorServiciosController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		/*$conexión = new Mongodb( "mongodb://192.168.5.243" ); // conectar a un host remoto (puerto predeterminado: 27017)
		$bd = $conexión->tvshows;
		$colección = $bd->tvshow;
		$cursor = $colección->find();
		print_r($cursor);*/
		$par = Catalogo::OrderBy('ctp_id', 'asc')->pluck('ctp_descripcion', 'ctp_id');
		//dd($par);
		if (Auth::user()->usr_id != Session::get('ID_USUARIO')) {return view('auth.login');} else {
			return view('backend.administracion.parametricas.motorServicios.index',compact('par'));
		}
	  }

	public function lst_catalogo(Request $data) {
		$success   = array("code"    => 200);
		$error     = array("message" => "error de instancia", "code" => 602);
		$respuesta = '';
		try {
			$success = array("code" => 200);
			$data    = \DB::select('select * from sp_lst_catalogo()');
		
			return response()->json(["data" => $data, "success" => $success]);
		} catch (Exception $error) {
			//dd('->', $error);
			return response()->json(["error" => $error]);

		}
	}
	public function lst_motor_servicios(Request $data) {
		if (Auth::user()->usr_id != Session::get('ID_USUARIO')) {return view('auth.login');} else {
			$data  = \DB::select('select * from sp_lst_servicio2()');
			$data2 = new Collection($data);
			return Datatables::of($data2)
				->editColumn('acciones', '@if ($_srv_tipo_servicio == "C")
                <button class="btncirculo" style="background:#29b6f6" data-target="#modalUpdate" data-toggle="modal" data-placement="top" onClick="editarServicio({{$_srv_id}});" title="Modificar" type="button"><i class="glyphicon glyphicon-pencil"></i></button><button  class="btncirculo "   style="background:#ef5350" onClick="darBajaMotorServicio({{$_srv_id}});" data-placement="top" title="Eliminar"><i class="glyphicon glyphicon-trash"></i></button>
                @else
                <button class="btncirculo" style="background:#ffa726" data-target="#modalServicio" data-toggle="modal" data-placement="top" onClick="cargarServicio({{$_srv_id}});" title="Ejecutar Servicio" type="button"><i class="fa fa-rocket"></i></button><button class="btncirculo" style="background:#29b6f6" data-target="#modalUpdate" data-toggle="modal" data-placement="top" onClick="editarServicio({{$_srv_id}});" title="Modificar" type="button"><i class="glyphicon glyphicon-pencil"></i></button><button  class="btncirculo "   style="background:#ef5350" onClick="darBajaMotorServicio({{$_srv_id}});" data-placement="top" title="Eliminar"><i class="glyphicon glyphicon-trash"></i></button>
                @endif')
				->editColumn('_EnSPConn', '@if ($_srv_tipo_servicio == "R")
                {{$_srv_endpoint}}   Metodo: {{$_srv_verbo}}
            @elseif ($_srv_tipo_servicio == "S")
                {{$_srv_sp}}   DB: {{$_srv_bd}}
            @elseif ($_srv_tipo_servicio == "C")
                Conexión: {{$_srv_sp}}
            @endif')
				->editColumn('_tipo', '@if ($_srv_tipo_servicio == "R")
                <i class="fa fa-circle" style="color:#64dd17"></i>&nbsp;REST
            @elseif ($_srv_tipo_servicio == "S")
                <i class="fa fa-circle" style="color:#ff9800"></i>&nbsp;SP
            @elseif ($_srv_tipo_servicio == "C")
                <i class="fa fa-circle" style="color:#03A9F4"></i>&nbsp;CONN
            @endif')
				->addColumn('_srv_arg', function ($data2) {
					$recopilado = '';
					foreach (json_decode($data2->_srv_argumentos) as $lin => $val) {
						$recopilado .= '('.$lin.'='.$val.') ';
					}
					return $recopilado;
				})
				->make(true);
		}

	}

	public function get_restDinamico($id) {
		$success   = array("code"    => 200);
		$error     = array("message" => "error de instancia", "code" => 602);
		$respuesta = '';
		try {
			$success = array("code" => 200);
			$data    = \DB::select('select * from sp_get_servicio_id(?)', array($id));
			if ($data[0]->_ser_tipo_servicio == 'R') {
				$endponit   = $data[0]->_ser_endpoint;
				$verbo      = $data[0]->_ser_endpoint_verbo;
				$argumentos = $data[0]->_ser_argumentos;
			}
			return response()->json(["data" => $data, "success" => $success]);
		} catch (Exception $error) {
			dd('->', $error);
			return response()->json(["error" => $error]);

		}
	}

	public function darBajaMotorServicio(Request $data) {
		$data = \DB::select('select * from sp_baja_servicio(?)', array($data['idServicio']));
		return json_encode($data, true);
	}
	///////////////////////////////////////////////////////////////////////////////////////////
	public function actualizarServicio(Request $data) {
		$dato  = array();
		$dato2 = array();
		foreach (json_decode($data['datos'], true) as $indice => $valor) {
			if ($indice != 'srv_argumentos') {
				if ($indice == 'srv_clave' && $valor != '') {$valor = $valor;}//{$valor=Crypt::encrypt($valor);}
				$dato[$indice]                                      = $valor;
			} else {
				$inde                                                                      = count($valor);
				if ($inde != 0) {foreach ($valor as $indiceA => $valorA) {$dato2[$indiceA] = $valorA;}$dato[$indice] = $dato2;} else { $dato[$indice] = $valor;}
			}
		}
		$data = \DB::select('select * from sp_update_servicio(?,?,?,?,?)', array(
				$data['id']
				, json_encode($dato)
				, $data['propiedades']
				, 'A'
				, 1));
		return json_encode($data, true);
	}

	public function get_servicioID($id) {
		$data  = \DB::select('select * from sp_get_servicio_id(?)', array($id));
		$dato  = array();
		$dato2 = array();
		$cn    = 0;
		//echo 'HOLA11'. Crypt::decrypt($data[0]->_ser_clave);
		if ($data[0]->_ser_clave != '') {
			if (password_verify('rasmuslerdorf', $data[0]->_ser_clave)) {
				$data[0]->_ser_clave = $data[0]->_ser_clave;
			} else {
				$data[0]->_ser_clave = $data[0]->_ser_clave;
				//Crypt::decrypt($data[0]->_ser_clave);
			}
		}

		return $data;
	}

	public function get_restDinamico_CURL(Request $request) {
		$argumento       = json_decode($request['argumentos'], true);
		$clienteServicio = new \Ixudra\Curl\CurlService();
		switch ($request['metodo']) {
			case 'POST':
				$cliente = $clienteServicio->to($request['url'])
				                           ->withContentType('application/json')
				                           ->withData($argumento)->asJson()->post();
				break;
			case 'GET':
				$cliente = $clienteServicio->to($request['url'])
				                           ->withContentType('application/json')
				                           ->withData($argumento)->asJson()->get();
				break;
			case '':

				break;
		}
		$resultado = new collection($cliente);
		return $resultado;
		//Datatables::of($resultado)->make(true);
	}

	public function get_restDinamico_servicio(Request $request) {
		$gestor     = $request['gestor'];
		$servicio   = $request['servicio'];
		$arg        = $request['argumento'];
		$paramEnvio = "";
		if (count($arg) != 0) {
			$parametros = "";
			foreach ($arg as $lin) {
				$parametros .= $lin.",";
			}
			$ln         = strlen($parametros);
			$paramEnvio = substr($parametros, 0, $ln-1);
		}
		if ($gestor != "mssql") {$sql = "SELECT * from $servicio($paramEnvio);";} else { $sql = "EXEC dbo.$servicio $paramEnvio;";}
		try {
			$db   = self::getDB($gestor, $request['host'], $request['usuario'], $request['clave'], $request['bd_nombre'], $request['puerto']);
			$stmt = $db->prepare($sql);
			$stmt->execute();
			$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
			$dato = 0;
			if (sizeof($rows) > 0) {
				$array = $rows;
			} else {
				$array[0] = array(
					"error" => "No se pudo realizar el servicio",
				);
			}
			return $array;
		} catch (PDOException $e) {
			$error = array('error' => $e->getMessage());
			return $error;
		}
	}

	function getDB($base, $host, $usuario, $clave, $nombrebd, $puerto) {
		switch ($base) {
			case "mysql":
				$dbConnection = new PDO("mysql:host=$host;dbname=$nombrebd;port=$puerto", $usuario, $clave);

				break;
			case "postgres":
				$dbConnection = new PDO("pgsql:dbname=$nombrebd;host=$host;port=$puerto", $usuario, $clave);

				break;
			case "mssql":
				$dbConnection = new PDO("dblib:host=$host;dbname=$nombrebd", $usuario, $clave);
				break;
			case "mongodb":
				$conexión = new Mongodb("mongodb://$host");
				$bd        = $conexión->$nombrebd;
				break;
		}
		if ($base == "mongodb") {$dbConnection = json_decode($db);return $dbConnection;} else {
			$dbConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			return $dbConnection;}
	}
	
	public function insertarServicio(Request $request) {
		$dato  = array();
		$dato2 = array();
		foreach (json_decode($request['dataServicio'], true) as $indice => $valor) {
			if ($indice != 'srv_argumentos') {
				if ($indice == 'srv_clave' && $valor != '') {$valor = $valor;}//{$valor=Crypt::encrypt($valor);}
				$dato[$indice]                                      = $valor;
			} else {
				$inde                                                                      = count($valor);
				if ($inde != 0) {foreach ($valor as $indiceA => $valorA) {$dato2[$indiceA] = $valorA;}$dato[$indice] = $dato2;} else { $dato[$indice] = $valor;}
			}
		}


		$data = \DB::select('select sp_insert_servicio(?,?,?,?,?)', array(json_encode($dato), $request['propiedades'], 1,$request['workspace'],$request['tiposervicio']));
		return $data;
	}

	function llamarConexionDB(Request $request) {
		$data   = \DB::select("select * from sp_obtener_servicio(?)", array($request["id"]));
		$base   = $data[0]->_ser_gestor;
		$dbhost = $data[0]->_ser_host;
		$dbuser = $data[0]->_ser_usuario;
		$dbpass = $data[0]->_ser_clave;
		//Crypt::decrypt($data[0]->_ser_clave);
		$dbname              = $data[0]->_ser_bd;
		$dbpuerto            = $data[0]->_ser_puerto;
		$dbConnSTR           = $data[0]->_ser_sp;
		$data[0]->_ser_clave = $dbpass;
		try {
			switch ($base) {
				case "mysql":
					$dbConnSTR = "mysql:host=$dbhost;dbname=$dbname;port=$dbpuerto";

					break;
				case "postgres":
					$dbConnSTR = "pgsql:dbname=$dbname;host=$dbhost;port=$dbpuerto";

					break;
				case "mssql":
					$dbConnSTR = "dblib:host=$dbhost;dbname=$dbname";
					break;
				case "mongodb":
					$dbConnSTR = "mongodb://$host";
					break;
			}
			if ($base == "mongodb") {
				$conexión = new Mongodb($dbConnSTR);
				$dbConn    = $conexión->$nombrebd;
			} else {

				$dbConn = new PDO($dbConnSTR, $dbuser, $dbpass);
				$dbConn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			}

			if ($base == 'mssql') {$sql = "SELECT  ROUTINE_NAME from    INFORMATION_SCHEMA.ROUTINES where   ROUTINE_TYPE = 'PROCEDURE' order by ROUTINE_NAME ;";} elseif ($base == 'postgres') {$sql = "SELECT proname,proargnames FROM pg_catalog.pg_namespace n JOIN pg_catalog.pg_proc p ON pronamespace = n.oid WHERE nspname = 'public';";} elseif ($base == 'mysql') {$sql = "SELECT specific_name FROM information_schema.routines;";} elseif ($base == 'mongodb') {$sql = "SELECT specific_name FROM information_schema.routines;";}
			$array;
			$stmt;
			$stmt = $dbConn->prepare($sql);
			$stmt->execute();
			$rows                          = $stmt->fetchAll(PDO::FETCH_ASSOC);
			if (sizeof($rows) > 0) {$array = $rows;} else { $array[0] = array("Query" => "Sin resultados.");}
			$data[0]->_ser_sp              = $array;
			$dbConn                        = null;
			$stmt                          = null;
			return $data;
			//json_encode($dato);
		} catch (PDOException $e) {
			$dbConn = null;
			$stmt   = null;
			die("Sin conecxión: ".$e->getMessage());
			return json_encode(fale);
		}
	}

	public function lst_servicio1(Request $data) {
		//dd($data);
		//$data['idSucursal']
		$value= $data['Workspace'];
		//dd($value);
		//echo Auth::user()->usr_id.'<>'.Session::get('ID_USUARIO');
		if (Auth::user()->usr_id != Session::get('ID_USUARIO')) {return view('frontend.login');} else {
			$data  = \DB::select('select * from sp_lst_servicio2(?)', array($value));
			//dd($data);
			$data2 = new Collection($data);
			//dd($data2);
			return response()->json(["Mensaje" => $data2]);
			/*return Datatables::of($data2)
				->editColumn('acciones', '<button class="btncirculo" style="background:#29b6f6" fa fa-plus-square pull-right" data-target="#modalUpdate" data-toggle="modal" data-placement="top" onClick="lst_codigo({{$_scd_id}})" title="Modificar" type="button"><i class="glyphicon glyphicon-pencil"></i></button> <button class="btncirculo" style="background:#ef5350" onClick="darBajaRegladeNegocio({{$_scd_id}});" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="glyphicon glyphicon-trash"></i></button>')
				->addColumn('_scd_codigo', function ($data2) {
					$recopilado = substr($data2->_scd_data, 17, 10).'  ...';
					return $recopilado;
					;

				})
				->make(true);*/
		}
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		//
	}
}
