<?php

namespace gamlp\Http\Controllers\Auth;

use gamlp\Http\Controllers\Controller;
use gamlp\Modelo\admin\RolUsuario;
use gamlp\Modelo\admin\Usuario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Session;

class AuthController extends Controller {
	public function postLogin() {
		$conect = input::only('usr_usuario', 'usr_clave');
		if (Auth::attempt($conect, true)) {
			$ids     = Auth::user()->usr_id;
			$usuario = RolUsuario::getusuarios($ids);
			foreach ($usuario as $row) {
				Session::put('USUARIO', $row->vusr_usuario);
				Session::put('AUTENTICADO', true);
				Session::put('ROL', $row->vusr_usuario);
				Session::put('ID_ROL', $row->vrls_id);
				Session::put('NOMBRES', $row->vprs_nombres);
				Session::put('PATERNO', $row->vprs_paterno);
				Session::put('MATERNO', $row->vprs_materno);
				Session::put('ID_USUARIO', $row->vprs_id);
			}
			return Redirect::to('/home');
		} else {
			return view('frontend.login');
		}
	}

	public function showLoginForm() {
		$view = property_exists($this, 'loginView')
		?$this->loginView:'auth.authenticate';

		if (view()->exists($view)) {
			return view($view);
		}

		return view('frontend.login');
	}

	public function close() {
		Auth::logout();
		return view('frontend.bienvenida');
	}

	public function Login() {
		return view('frontend.login');
	}

	public function create(Request $data) {
		try {
			$user = Usuario::create([
					'usr_usuario' => $data['usuario'],
					'usr_clave'   => bcrypt($data['password']),
					'usr_prs_id'  => 1,
					'usr_usr_id'  => 1
				]);
			session::flash("message", "Se ha registrado el usuario ".$user->usr_usuario." de manera exitosa!");
			return view('admin.landing');
		} catch (Exception $e) {
			return view('errores.404');
		}
	}
}
