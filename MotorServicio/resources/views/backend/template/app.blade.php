<!DOCTYPE html>
<html lang="en">
    @section('htmlheader')
        @include('backend.template.partials.htmlheader')
    @show
    <body class="skin-green sidebar-mini">
        <div class="wrapper">
            @include('backend.template.partials.mainheader')
            @include('backend.template.partials.sidebar')
            <div class="content-wrapper">
                <section class="container-fluit">
                    @yield('main-content')
                </section>
            </div>
            @include('backend.template.partials.controlsidebar')
            @include('backend.template.partials.footer')
        </div>
        @section('scripts')
            @include('backend.template.partials.scripts')
        @show
    </body>
</html>