{!! Html::script('js/jquery-3.1.0.min.js') !!}
{!! Html::script('js/loadingoverlay.js')!!}

{!! Html::script('js/bootstrap.min.js') !!}

{!! Html::script('js/app.min.js') !!}

{!! Html::script('js/bootstrapValidator.js') !!}

{!! Html::script('js/sweetalert.min.js') !!}
{!! Html::script('js/datatable/jquery.dataTables.min.js') !!}
{!! Html::script('js/datatable/dataTables.fixedHeader.min.js') !!}
{!! Html::script('js/datatable/dataTables.responsive.min.js') !!}


{!! Html::script('js/datatable/dataTables.buttons.min.js') !!}
{!! Html::script('js/datatable/buttons.html5.min.js')!!}



@stack('scripts')
