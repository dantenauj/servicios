@inject('menu','gamlp\Http\Controllers\MenuController')
<aside class="main-sidebar">
                <section class="sidebar">
                    <ul class="sidebar-menu">
                       <div class="user-panel" align="center">
        <div class="pull-center image">
          <img src="img/puntocinco04.jpeg" class="img-circle" alt="User Image">
        </div>

      </div>

                        @foreach ($menu->submenus() as $link01)
                            @php
                                $grupo01=0;
                                $grupo01= $link01->grp_id;
                            @endphp
                        <li class="treeview">
                            <a href="#">
                                <i class="{{$link01->grp_imagen}}">
                                </i>
                                <span class="pull-right-container">
                                    {{ $link01->grp_grupo }}
                                </span>
                                <i class="fa fa-angle-left pull-right">
                                </i>
                            </a>
                            <ul class="treeview-menu menu-open">
                            @foreach ($menu->links($grupo01) as $link02)

                                <li class="active">
                                    <a href="{{ url($link02->opc_contenido) }}">
                                        <i class="fa fa-database" aria-hidden="true" ></i>
                                        {{ $link02->opc_opcion }}
                                    </a>
                                </li>
                            @endforeach
                            </ul>
                        </li>
                        @endforeach
                    </ul>
                    <!-- /.sidebar-menu -->
                </section>
                <!-- /.sidebar -->
            </aside>

@push('scripts')
<script>
</script>
@endpush