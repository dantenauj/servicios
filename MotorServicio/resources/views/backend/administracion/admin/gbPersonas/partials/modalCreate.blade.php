<div class="modal fade modal-default" data-backdrop="static" data-keyboard="false" id="myCreate" tabindex="-5">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
      <div class="row">
         <div class="col-xs-12 container-fluit">
             <div class="panel panel-warning">
                <div class="panel-heading">
                    <h4>
                    Registrar Persona
                    </h4>
                    </div>
                    <div class="panel-body">
                <div class="caption">
                    <hr>
                        {!! Form::open(['id'=>'persona'])!!}
                        <input id="token" name="csrf-token" type="hidden" value="{{ csrf_token() }}">
                            <input id="id" name="prsid" type="hidden" value="">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label>
                                                    Nombre:
                                                </label>
                                                <span class="block input-icon input-icon-right">
                                                    {!! Form::text('prs_nombres', null, array('placeholder' => 'ingrese Nombre(s) ','maxlength'=>'20','class' => 'form-control','id'=>'nombres')) !!}
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label>
                                                    Paterno:
                                                </label>
                                                <span class="block input-icon input-icon-right">
                                                    {!! Form::text('prs_paterno', null, array('placeholder' => 'Ingrese Apellido Paterno','maxlength'=>'15','class' => 'form-control','id'=>'paterno')) !!}
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label>
                                                    Materno:
                                                </label>
                                                <span class="block input-icon input-icon-right">
                                                    {!! Form::text('prs_materno', null, array('placeholder' => 'Ingrese Apellido Materno','maxlength'=>'15','class' => 'form-control','id'=>'materno')) !!}
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label>
                                                    C.I.:
                                                </label>
                                                <span class="block input-icon input-icon-right">
                                                    {!! Form::text('prs_ci', null, array('placeholder' => 'Ingrese C.I. ','maxlength'=>'10','class' => 'form-control','id'=>'ci')) !!}
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label>
                                                    Genero:
                                                </label>
                                                <div class="controls">
                                                    <!--{!! Form::text('prs_sexo', null, array('placeholder' => 'Ingrese genero','class' => 'form-control','id'=>'sexo')) !!}-->
                                                    {!! Form::radio('prs_sexo','M', ['class'=>'form-control','id'=>'sexo']) !!} Masculino 
                                           
                                    {!! Form::radio('prs_sexo','F',['class'=>'form-control','id'=>'sexo']) !!} Femenino
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label>
                                                    Fecha de Nacimiento:
                                                </label>
                                                <span class="block input-icon input-icon-right">
                                                    <div class="input-group">
                                                        <input class="form-control datepicker" id="fec_nacimiento" name="fec_nacimiento" type="text" value="">
                                                            <div class="input-group-addon">
                                                                <span class="glyphicon glyphicon-calendar">
                                                                </span>
                                                            </div>
                                                        </input>
                                                    </div>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label>
                                                    Dirección:
                                                </label>
                                                <span class="block input-icon input-icon-right">
                                                    {!! Form::text('prs_direccion', null, array('placeholder' => 'Ingrese dirección','maxlength'=>'50','class' => 'form-control','id'=>'direccion')) !!}
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label>
                                                    Dirección Auxiliar:
                                                </label>
                                                <span class="block input-icon input-icon-right">
                                                    {!! Form::text('prs_direccion2', null, array('placeholder' => 'Ingrese dirección auxiliar','class' => 'form-control','id'=>'direccionaux')) !!}
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label>
                                                    Telefono:
                                                </label>
                                                <span class="block input-icon input-icon-right">
                                                    {!! Form::text('prs_telefono', null, array('placeholder' => 'Ingrese telefono', 'class' => 'form-control','id'=>'telefono')) !!}
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label>
                                                    Telefono Auxiliar:
                                                </label>
                                                <span class="block input-icon input-icon-right">
                                                    {!! Form::text('prs_telefono2', null, array('placeholder' => 'Ingrese telefono auxiliar', 'class' => 'form-control','telefonoaux')) !!}
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label>
                                                    Celular:
                                                </label>
                                                <span class="block input-icon input-icon-right">
                                                    {!! Form::text('prs_celular', null, array('placeholder' => 'Ingrese celular ', 'class' => 'form-control','id'=>'celular')) !!}
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label>
                                                    Empresa Telefonica:
                                                </label>
                                                <select class="form-control" id="emptelefonica" name="prs_empresa_telefonica" placeholder="Ingrese empresa telefonica" value="">
                                                    <option value="Entel">
                                                        Entel
                                                    </option>
                                                    <option value="Viva">
                                                        Viva
                                                    </option>
                                                    <option value="Tigo">
                                                        Tigo
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label>
                                                    Correo:
                                                </label>
                                                <span class="block input-icon input-icon-right">
                                                    {!! Form::text('prs_correo', null, array('placeholder' => 'Ingrese correo ejemplo@gmail.com','class' => 'form-control','id'=>'correo')) !!}
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label>
                                                    Estado Civil:
                                                </label>
                                                <span class="block input-icon input-icon-right">
                                                    <select class="form-control" id="estadocivil" name="prs_id_estado_civil" placeholder="Ingrese estado civil">
                                                        @foreach($data as $rol)
                                                        <option value="{{$rol->estcivil_id}}">
                                                            {{$rol->estcivil}}
                                                        </option>
                                                        @endforeach
                                                    </select>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </input>
                        </input>
                    </hr>
                </div>
            </div>
            </div>
            </div>
            </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-default" data-dismiss="modal" type="button">
                    Cerrar
                </button>
                {!!link_to('#',$title='Registrar', $attributes=['id'=>'registro','class'=>'btn btn-warning'], $secure=null)!!}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>