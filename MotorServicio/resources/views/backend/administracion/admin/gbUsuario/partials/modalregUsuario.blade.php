<div class="modal fade modal-default" data-backdrop="static" data-keyboard="false" id="myUserCreate" tabindex="-5">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
         <div class="modal-body">
          <div class="row">
           <div class="col-xs-12 container-fluit">
               <div class="panel panel-warning">
                <div class="panel-heading">
                    <h4>
                        Registrar Usuario
                    </h4>
                </div>
                <div class="panel-body"> 
                    <div class="caption">
                        <hr>
                        {!! Form::open(['id'=>'regUser'])!!}
                        <input id="token" name="csrf-token" type="hidden" value="{{ csrf_token() }}">
                        <input id="id" name="usr_id" type="hidden" value="">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <input id="token" name="csrf-token" type="hidden" value="{{ csrf_token() }}">
                                <div class="form-group">
                                    <label class="control-label">
                                        Nombre:
                                    </label>
                                    {!! Form::text('usr_usuario', null, array('placeholder' => 'Nombre de Opcion','class' => 'form-control','name'=>'usr_usuario','id'=>'usuario')) !!}
                                </div>
                                <div class="form-group">
                                    <label class="control-label">
                                        Contraseña:
                                    </label>
                                    {!! Form::text('usr_clave', null, array('placeholder' => 'Ingrese la Contraseña','class' => 'form-control','name'=>'usr_clave','id'=>'clave')) !!}
                                </div>
                                <div class="form-group">
                                    <label>
                                        Grupo:
                                    </label>
                                    {!! Form::Label('item','Personas:')!!}
                                    {!! Form::select('prs_id', $persona, null,['class'=>'form-control','name'=>'usr_prs_id', 'id'=>'usr_prs_id']) !!}
                                </div>
                            </input>
                        </div>

                    </div>
                </input>
            </input>
        </hr>
    </div>
</div>
</div>
</div>
</div>
<div class="modal-footer">
    <button class="btn btn-default" data-dismiss="modal" type="button">
        Cerrar
    </button>
    {!!link_to('#',$title='Registrar', $attributes=['id'=>'registroUsuario','class'=>'btn btn-warning'], $secure=null)!!}                 
    {!! Form::close() !!}
</div>
</div>
</div>
</div>
</div>
