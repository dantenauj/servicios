@extends('frontend.auth')
@section('content')
<body class="hold-transition login-page ">
    <div class="login-box">
        <div class="login-logo">
            <a href="{{ url('/home') }}">
                <img src="{{asset('/img/puntocinco04.jpeg')}}" width="100" height="100" align="left" alt="" />
            </a>
        </div>
        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong>
            {{trans('adminlte_lang::message.someproblems') }}
            <br><br>
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        <div class="login-box-body panel panel-info">
            <br>
            <h2><p class="login-box-msg">Iniciar Session</p></h2>
            <form action="{{ route('login-post') }}" method="post"  >
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div style="margin-bottom: 25px" class="form-group has-feedback">
                        <input type="text" class="form-control" placeholder="INGRESE SU USUARIO" name="usr_usuario"/>
                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                    </div>
                    <div style="margin-bottom: 25px" class="form-group has-feedback">
                        <input type="password" class="form-control" placeholder="INGRESE SU CONTRASEÑA" name="usr_clave"/>
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>
                <div class="row">

                    <div class="col-md-2 text-center">
                    </div>
                    <div class="col-md-8 text-center">
                        <button type="submit" class="btn btn-lg btn-success btn-sm">Iniciar Sesión</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</body>
@endsection