<!DOCTYPE html>
<html lang="en-US">
   <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width" />
      <title>Motor de Servicios GAMLP</title>
      <link rel="stylesheet" href="css/components.css">
      <link rel="stylesheet" href="css/responsee.css">
      <link rel="stylesheet" href="owl-carousel/owl.carousel.css">
      <link rel="stylesheet" href="owl-carousel/owl.theme.css">
      <!-- CUSTOM STYLE -->
      <link rel="stylesheet" href="css/template-style.css">
      <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
      <script type="text/javascript" src="js/jquery-1.8.3.min.js"></script>
      <script type="text/javascript" src="js/jquery-ui.min.js"></script>
      <script type="text/javascript" src="js/modernizr.js"></script>
      <script type="text/javascript" src="js/responsee.js"></script>
      <script type="text/javascript" src="js/template-scripts.js"></script>

      <!--[if lt IE 9]>
          <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
      <![endif]-->
   </head>
   <body class="size-1140">
      <!-- TOP NAV WITH LOGO -->
      <header>
         <div id="topbar">
            <div class="line">

               <div class="s-12 m-6 l-6">
                  <div class="social right">
                     <!--a><i class=""></i></a> <a><i class="icon-twitter_circle"></i></a> <a><i class="icon-google_plus_circle"></i></a> <a><i class="icon-instagram_circle"></i></a-->
                  </div>
               </div>
            </div>
         </div>
         <nav>
            <div class="line">
               <div class="s-12 l-2">
                  <p class="logo"><strong>Motor de Servicios</strong> GAMLP</p>
               </div>
               <div class="top-nav s-12 l-10">
                  <p class="nav-text">Menu</p>
                  <ul class="right">
                     <li class="active-item"><a href="#carousel">Inicio</a></li>
                     <li><a href="sesion">Iniciar Sesion</a></li>
                  </ul>
               </div>
            </div>
         </nav>
      </header>
      <section>


       <div class="s-6 m-6 l-6">
 <!-- CAROUSEL -->
         <div id="carousel">
            <div id="owl-demo" class="owl-carousel owl-theme">
               <div class="item">
                  <img src="img/imagen-fondolapaz.jpg" style="width: 1000px;height: 650px;">
                  <div class="line">
                     <div class="text hide-s">

                     </div>
                  </div>
               </div>
               <div class="item">
                  <img src="img/banner1.jpg" style="width: 1000px;height: 650px;">
                  <div class="line">
                     <div class="text hide-s">
                        <h2>Tareas del motor de servicios</h2>
                        <p>Diseñar y crear una base de datos que contenga las tablas relacionales o los documentos XML que el sistema necesita.</p>
                     </div>
                  </div>
               </div>
            </div>
         </div>
       </div>
  <div class="s-6 m-6 l-6">
   <!-- MAP -->
   <img src="img/imagen-fondolapaz.jpg" style="width: 5000px;height: 6050px;">
         <!--div id="map-block">
            <iframe style="width: 1000px;height: 650px;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1247814.3661917313!2d16.569872019090596!3d48.23131953825178!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x476c8cbf758ecb9f%3A0xddeb1d26bce5eccf!2sGallayova+2150%2F19%2C+841+02+D%C3%BAbravka!5e0!3m2!1ssk!2ssk!4v1440344568394" width="100%" height="450" frameborder="0" style="border:0"></iframe>
         </div-->
  </div>





         <!-- FIRST BLOCK -->
         <div id="first-block">
            <div class="line">
               <h1>Proporcionar soporte técnico administrativo diario para optimizar el rendimiento de la base de datos.</h1>
               <div class="s-12 m-4 l-2 center"><a class="white-btn" href="sesion">Ingresar al Sistema</a></div>
            </div>
         </div>


      </section>
      <!-- FOOTER -->
      <footer>
         <div class="line">
            <div class="s-12 l-6">
               <p>Copyright 2017, GAMLP</p>
            </div>
         </div>
      </footer>
      <script type="text/javascript" src="owl-carousel/owl.carousel.js"></script>
      <script type="text/javascript">
         jQuery(document).ready(function($) {
            var theme_slider = $("#owl-demo");
            $("#owl-demo").owlCarousel({
                navigation: false,
                slideSpeed: 300,
                paginationSpeed: 400,
                autoPlay: 6000,
                addClassActive: true,
             // transitionStyle: "fade",
                singleItem: true
            });
            $("#owl-demo2").owlCarousel({
                slideSpeed: 300,
                autoPlay: true,
                navigation: true,
                navigationText: ["&#xf007","&#xf006"],
                pagination: false,
                singleItem: true
            });

            // Custom Navigation Events
            $(".next-arrow").click(function() {
                theme_slider.trigger('owl.next');
            })
            $(".prev-arrow").click(function() {
                theme_slider.trigger('owl.prev');
            })
        });
      </script>
   </body>
</html>