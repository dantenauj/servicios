
CREATE TABLE s_servicios
    (
    srv_id serial PRIMARY KEY NOT NULL,
    srv_data jsonb DEFAULT '{"srv_tipo_servicio": "","srv_bd": "","srv_gestor": "","srv_puerto": "","srv_host": "", "srv_sp": "", "srv_endpoint": "", "srv_endpoint_verbo": "", "srv_argumentos": "", "srv_usuario": "0", "srv_clave": ""}'::jsonb,
    srv_propiedades jsonb DEFAULT '{"srv_nombre": "","srv_descripcion": ""}'::jsonb,
    srv_estado character(1) NOT NULL DEFAULT 'A'::bpchar,
    srv_usr_id integer,
    srv_registrado timestamp without time zone NOT NULL DEFAULT now(),
    srv_modificado timestamp without time zone NOT NULL DEFAULT now()
    )
  WITH (OIDS=FALSE);
ALTER TABLE s_servicios
  OWNER TO postgres;

INSERT INTO s_servicios(srv_data,srv_propiedades) VALUES
    ('{"srv_bd": "comercio","srv_gestor": "postgres", "srv_sp": "sp_obt_titular", "srv_clave": "eyJpdiI6IlN5K1RabmdpZUpTY0dGWmU0alJhNXc9PSIsInZhbHVlIjoiQzdOUlh1V0ZaXC9JME9uTnNxdGxHcmc9PSIsIm1hYyI6IjU3NGFkZDA1OTFmNjhhMmMyOTgwMzlmNzY5NjVmM2Y4NjJjM2ZiNWNkOTkwZjlkZTE2MWQ0YmQyZTM0Yzc5MWYifQ==","srv_puerto": "5432","srv_host": "localhost", "srv_usuario": "postgres", "srv_endpoint": "", "srv_argumentos": {"ci":"''123456''"}, "srv_tipo_servicio": "S", "srv_endpoint_verbo": "POST"}','{"srv_nombre": "Prueba SP","srv_descripcion": "directo"}'),
    ('{"srv_bd": "","srv_gestor": "", "srv_sp": "", "srv_clave": "", "srv_usuario": "", "srv_endpoint": "http://192.168.5.141:9098/v.01/Territorial/getDatosInmueble", "srv_argumentos": {"sIdFichaTecnica": "4512"}, "srv_tipo_servicio": "R", "srv_endpoint_verbo": "POST"}','{"srv_nombre": "Prueba Rest2","srv_descripcion": "Funciona"}'),
    ('{"srv_bd": "", "srv_sp": "", "srv_host": "", "srv_clave": "", "srv_gestor": "", "srv_puerto": "", "srv_usuario": "", "srv_endpoint": "http://localhost:9092/wsAlmacen/lstSolicitud", "srv_argumentos": {"estado": "A"}, "srv_tipo_servicio": "R", "srv_endpoint_verbo": "POST"}','{"srv_nombre": "Prueba Rest 3","srv_descripcion": "Solo para varios argumentos"}'),
    ('{"srv_bd": "almacenes", "srv_sp": "sp_insert_prueba", "srv_host": "localhost", "srv_clave": "eyJpdiI6IlN5K1RabmdpZUpTY0dGWmU0alJhNXc9PSIsInZhbHVlIjoiQzdOUlh1V0ZaXC9JME9uTnNxdGxHcmc9PSIsIm1hYyI6IjU3NGFkZDA1OTFmNjhhMmMyOTgwMzlmNzY5NjVmM2Y4NjJjM2ZiNWNkOTkwZjlkZTE2MWQ0YmQyZTM0Yzc5MWYifQ==", "srv_gestor": "postgres", "srv_puerto": "5432", "srv_usuario": "postgres", "srv_endpoint": "", "srv_argumentos": {"11": "''Pedrinchi''", "22": "25", "33": "''Miraflores''", "44": "''Registro''", "55": "1"}, "srv_tipo_servicio": "S", "srv_endpoint_verbo": "POST"}','{"srv_nombre": "Insertar prueba", "srv_descripcion": "PRUEBAS DE INSERT"}'),
    ('{"srv_bd": "", "srv_sp": "", "srv_host": "", "srv_clave": "", "srv_gestor": "", "srv_puerto": "", "srv_usuario": "", "srv_endpoint": "http://localhost:9092/wsAlmacen/abmInsertPrueba", "srv_argumentos": {"edad": "25", "nombre": "Pedrinchi", "user_id": "1", "actividad": "Registro", "direccion": "Miraflores"}, "srv_tipo_servicio": "R", "srv_endpoint_verbo": "POST"}','{"srv_nombre": "Insertar prueba REST", "srv_descripcion": "PRUEBAS DE INSERT"}'),
    ('{"srv_bd": "almacenes", "srv_sp": "sp_update_prueba", "srv_host": "localhost", "srv_clave": "eyJpdiI6IlN5K1RabmdpZUpTY0dGWmU0alJhNXc9PSIsInZhbHVlIjoiQzdOUlh1V0ZaXC9JME9uTnNxdGxHcmc9PSIsIm1hYyI6IjU3NGFkZDA1OTFmNjhhMmMyOTgwMzlmNzY5NjVmM2Y4NjJjM2ZiNWNkOTkwZjlkZTE2MWQ0YmQyZTM0Yzc5MWYifQ==", "srv_gestor": "postgres", "srv_puerto": "5432", "srv_usuario": "postgres", "srv_endpoint": "", "srv_argumentos": {"11": "3", "22": "''Bernart''", "33": "26", "44": "''Miraflore Alto''", "55": "''Actualizado''", "66": "''A''", "77": "1"}, "srv_tipo_servicio": "S", "srv_endpoint_verbo": "POST"}','{"srv_nombre": "ACtualizar Prueba", "srv_descripcion": "Prueba Update"}'),
    ('{"srv_bd": "", "srv_sp": "", "srv_host": "", "srv_clave": "", "srv_gestor": "", "srv_puerto": "", "srv_usuario": "", "srv_endpoint": "http://localhost:9092/wsAlmacen/abmUpdatePrueba", "srv_argumentos": {"id": "4", "edad": "25", "estado": "A", "nombre": "Leonidas", "user_id": "1", "actividad": "administrador de BD", "direccion": "El Carmen"}, "srv_tipo_servicio": "R", "srv_endpoint_verbo": "POST"}','{"srv_nombre": "Actualiza PruebaRest", "srv_descripcion": "PRUEBA REST"}'),
    ('{"srv_bd": "almacenes", "srv_sp": "sp_lst_prueba", "srv_host": "localhost", "srv_clave": "eyJpdiI6IlN5K1RabmdpZUpTY0dGWmU0alJhNXc9PSIsInZhbHVlIjoiQzdOUlh1V0ZaXC9JME9uTnNxdGxHcmc9PSIsIm1hYyI6IjU3NGFkZDA1OTFmNjhhMmMyOTgwMzlmNzY5NjVmM2Y4NjJjM2ZiNWNkOTkwZjlkZTE2MWQ0YmQyZTM0Yzc5MWYifQ==", "srv_gestor": "postgres", "srv_puerto": "5432", "srv_usuario": "postgres", "srv_endpoint": "", "srv_argumentos": {}, "srv_tipo_servicio": "S", "srv_endpoint_verbo": "POST"}','{"srv_nombre": "Listar Pruebas", "srv_descripcion": "Llamar Lista"}'),
    ('{"srv_bd": "", "srv_sp": "", "srv_host": "", "srv_clave": "", "srv_gestor": "", "srv_puerto": "", "srv_usuario": "", "srv_endpoint": "http://localhost:9092/wsAlmacen/abmListarPrueba", "srv_argumentos": {}, "srv_tipo_servicio": "R", "srv_endpoint_verbo": "POST"}','{"srv_nombre": "Listar Pruebas REST", "srv_descripcion": "Lista General"}'),
    ('{"srv_bd": "almacenes", "srv_sp": "sp_get_prueba_id", "srv_host": "localhost", "srv_clave": "eyJpdiI6IlN5K1RabmdpZUpTY0dGWmU0alJhNXc9PSIsInZhbHVlIjoiQzdOUlh1V0ZaXC9JME9uTnNxdGxHcmc9PSIsIm1hYyI6IjU3NGFkZDA1OTFmNjhhMmMyOTgwMzlmNzY5NjVmM2Y4NjJjM2ZiNWNkOTkwZjlkZTE2MWQ0YmQyZTM0Yzc5MWYifQ==", "srv_gestor": "postgres", "srv_puerto": "5432", "srv_usuario": "postgres", "srv_endpoint": "", "srv_argumentos": {"id": "5"}, "srv_tipo_servicio": "S", "srv_endpoint_verbo": "POST"}','{"srv_nombre": "Cargar Dato ID", "srv_descripcion": "Datos del Usuario"}'),
    ('{"srv_bd": "", "srv_sp": "", "srv_host": "", "srv_clave": "", "srv_gestor": "", "srv_puerto": "", "srv_usuario": "", "srv_endpoint": "http://localhost:9092/wsAlmacen/abmGetIDPrueba", "srv_argumentos": {"id": "5"}, "srv_tipo_servicio": "R", "srv_endpoint_verbo": "POST"}','{"srv_nombre": "Carga Dato usuario REST", "srv_descripcion": "Datos generales del Usuario"}');


CREATE  TABLE s_codigo
(
 scd_id serial NOT NULL,
 scd_data jsonb DEFAULT '[{"scd_codigo": ""}]'::jsonb,
 scd_propiedades jsonb DEFAULT '{"scd_orden": "", "scd_nombre": "", "scd_version": "", "scd_descripcion": ""}'::jsonb,
 scd_estado character(1) NOT NULL DEFAULT 'A'::bpchar,
 scd_usr_id integer,
 scd_registrado timestamp without time zone NOT NULL DEFAULT now(),
 scd_modificado timestamp without time zone NOT NULL DEFAULT now(),
 CONSTRAINT s_codigo_pkey PRIMARY KEY (scd_id)
)
WITH (
 OIDS=FALSE
);
ALTER TABLE s_codigo
 OWNER TO postgres;

INSERT INTO s_codigo (scd_data, scd_propiedades) VALUES 
    ('[{"scd_codigo": "print &039;Hola mundo!&039;;"}]','{"scd_orden": "10", "scd_nombre": "Restar1", "scd_version": "0.01", "scd_descripcion": "Ejemplo 2"}'),
    ('[{"scd_codigo": "$ci = &039;11111222&039;;"}, {"scd_codigo": "$ci2 = &039;1111&039;;"}, {"scd_codigo": "$ci3 = $ci . $ci2;"}, {"scd_codigo": "echo &039;Finalmente&041;hr&042;&039;; echo &039;Se pudo!&041;br&042;&039;; echo &039;Resultado: &039; . $ci3 . &039;&041;br&042;&039;;"}]','{"scd_orden": "10", "scd_nombre": "Restar2", "scd_version": "0.01", "scd_descripcion": "Ejemplo 2"}'),
    ('[{"scd_codigo": "$a=2;"}, {"scd_codigo": "$b=3;"}, {"scd_codigo": "$c = $a + $b;"}, {"scd_codigo": "print &039;Sumatoria: &039; . $c;"}]','{"scd_orden": "10", "scd_nombre": "Restar3", "scd_version": "0.01", "scd_descripcion": "Ejemplo 2"}'),
    ('[{"scd_codigo": "$_conn=&039;pgsql:dbname=almacenes;host=localhost;port=5432&039;; $_userdb=&039;postgres&039;; $_passdb=&039;eyJpdiI6IlArekYzNFhlV0RGd2ZmRFU1WVlIUnc9PSIsInZhbHVlIjoiN2JDWTByTHN3ckFEcTFHbDhDSktCUT09IiwibWFjIjoiNzNjNTY3ODY4MjU1NDY5MTAzOGYyZTY3NGJkOWIzZTYwMWEyMDJiOGU2NjcyNzdkMTc3YTU1YTc0YzI3ZmQ5MCJ9&039;;\n$_tipo=&039;Update&039;;\n$dnom=&039;Datos Juan&039;; $dedad=&039;10&039;;$ddir=&039;Calle&039;;$dact=&039;Programar&039;;\n$_query=&040;INSERT INTO alm_prueba(alm_nombre,alm_edad,alm_direccion,alm_actividad,alm_usr_id)\n      VALUES (&039;&040;.$dnom.&040;&039;,&039;&040;.$dedad.&040;&039;,&039;&040;.$ddir .&040;&039;,&039;&040;.$dact .&040;&039;,1)&040;;\n$_dataDB=ejecutar_DB($_conn,$_query,$_userdb,$_passdb,$_tipo);\nif($_dataDB==true)\n{$res=&039;Se agregó un nuevo dato a la tabla&039;;}\nelse\n{$res=&039;Error al registrarlo&039;;}\necho $res;"}]','{"scd_orden": "10", "scd_nombre": "Insertar Dato Prueba", "scd_version": "1.0", "scd_descripcion": "Cuatro variables para realizar el Insert"}'),
    ('[{"scd_codigo": "$_conn=&039;pgsql:dbname=almacenes;host=localhost;port=5432&039;; $_userdb=&039;postgres&039;; $_passdb=&039;eyJpdiI6IlArekYzNFhlV0RGd2ZmRFU1WVlIUnc9PSIsInZhbHVlIjoiN2JDWTByTHN3ckFEcTFHbDhDSktCUT09IiwibWFjIjoiNzNjNTY3ODY4MjU1NDY5MTAzOGYyZTY3NGJkOWIzZTYwMWEyMDJiOGU2NjcyNzdkMTc3YTU1YTc0YzI3ZmQ5MCJ9&039;;\n$_tipo=&039;Update&039;;\n//$dnom=&039;Datos yooyoyo&039;; $dedad=&039;10&039;;$ddir=&039;Calle&039;;$dact=&039;Programar&039;;$did=&039;30&039;;\n$_query=&040;UPDATE alm_prueba SET alm_nombre=&039;&040;. $dnom .&040;&039; ,alm_edad=&040;.$dedad.&040;,alm_direccion=&039;&040;.$ddir .&040;&039; ,alm_actividad=&039;&040;.$dact .&040;&039; ,alm_modificado=now() WHERE alm_id=&040;.$did;\n$_dataDB=ejecutar_DB($_conn,$_query,$_userdb,$_passdb,$_tipo);\nif($_dataDB==true)\n{$res=&039;Se modifico la tabla&039;;}\nelse\n{$res=&039;Error al registrarlo&039;;}\necho &039;&041;br&042;&039;.$res;"}]','{"scd_orden": "10", "scd_nombre": "Actualizar Prueba ", "scd_version": "1.0", "scd_descripcion": "Agregar una variable ID a las variables parametro."}'),
    ('[{"scd_codigo": "$_dataS=ejecutar_SP(8);\n$d1=json_decode($_dataS,true);\n//echo sizeof($d1);\n//echo $d1[0][&039;lis_nombre&039;];\n$htmlListaCod= &039;&041;table id=&040;lsta-motoresbd&040; class=&040;thead-inverse table-container table-bordered&040; cellspacing=&040;1&040; width=&040;50%&040; &042;&039;;\n$htmlListaCod=$htmlListaCod .&039;&041;thead&042;&041;tr&042;&041;th&042;ID&041;/th&042;&041;th&042;Nombre&041;/th&042;&041;th&042;Edad&041;/th&042;&041;th&042;Dirección&041;/th&042;&041;/tr&042;&041;/thead&042;&039;;\nforeach($d1 as $posicion=&042;$jugador)\n {\n $htmlListaCod=$htmlListaCod . &039;&041;tr&042;&041;td align=&040;center&040;&042;&039; . $jugador[&039;lis_id&039;]. &039;&041;/td&042;&041;td&042;&039; . $jugador[&039;lis_nombre&039;]. &039;&041;/td&042;&041;td&042;&039; . $jugador[&039;lis_edad&039;]. &039;&041;/td&042;&041;td&042;&039; . $jugador[&039;lis_direccion&039;]. &039;&041;/td&042;&041;/tr&042;&039;;\n//echo $jugador[&039;lis_nombre&039;]. &039;&041;br&042;&039;;\n }\n$htmlListaCod=$htmlListaCod .&039;&041;/table&042;&039;;\necho $htmlListaCod;"}]','{"scd_orden": "10", "scd_nombre": "Listar los datos Prueba", "scd_version": "1.0", "scd_descripcion": "Cargar en una tabla lista de personas registradas"}');

CREATE OR REPLACE FUNCTION sp_get_servicio_id(IN id integer)
  RETURNS TABLE(_ser_id integer, _ser_tipo_servicio text, _ser_bd text, _ser_gestor text, _ser_puerto text, _ser_host text, _ser_sp text, _ser_endpoint text, _ser_endpoint_verbo text, _ser_argumentos text, _ser_usuario text, _ser_clave text, _ser_nombre text, _ser_descripcion text, _ser_tituloarg text) AS
$BODY$
  BEGIN
    RETURN QUERY 
      select srv_id,(srv_data->> 'srv_tipo_servicio':: text)as ser_tipo_servicio,(srv_data->> 'srv_bd':: text)as ser_bd,(srv_data->> 'srv_gestor':: text)as ser_gestor,(srv_data->> 'srv_puerto':: text)as ser_puerto,
      (srv_data->> 'srv_host':: text)as ser_host,(srv_data->> 'srv_sp':: text)as ser_sp,
      (srv_data->> 'srv_endpoint':: text)as ser_endpoint,(srv_data->> 'srv_endpoint_verbo':: text)as ser_endpoint_verbo,
      (srv_data->> 'srv_argumentos')as ser_argumentos,(srv_data->> 'srv_usuario':: text)as ser_usuario,(srv_data->> 'srv_clave':: text)as ser_clave, (srv_propiedades->> 'srv_nombre':: text)as ser_nombre,(srv_propiedades->> 'srv_descripcion':: text)as ser_descripcion,(srv_data->> 'srv_tituloarg':: text)as ser_tituloarg
      from s_servicios 
      where srv_estado = 'A' and srv_id=id
      order by srv_registrado desc;
  END;
  $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION sp_get_servicio_id(integer)
  OWNER TO postgres;


CREATE OR REPLACE FUNCTION sp_servicio_s()
  RETURNS TABLE(_ser_bsp text ,
_ser_bd text,
_ser_host text,
_ser_usuario text,
_ser_clave text,
_ser_puerto text,
_ser_argumentos text) AS
$BODY$
BEGIN
    RETURN QUERY select (srv_data->> 'srv_sp':: text)as ser_bsp,(srv_data->> 'srv_bd':: text)as ser_bd,(srv_data->> 'srv_host':: text)as ser_host,
    (srv_data->> 'srv_usuario':: text)as ser_usuario,(srv_data->> 'srv_clave':: text)as ser_clave,(srv_data->> 'srv_puerto':: text)as ser_puerto,
    (srv_data->> 'srv_argumentos')as ser_argumentos 
  from s_servicios 
  where (srv_data->> 'srv_tipo_servicio':: text) like 'S' and srv_estado like 'A' ;
END;
$BODY$
  LANGUAGE plpgsql 
COST 100;
ALTER FUNCTION sp_servicio_s()
  OWNER TO postgres;

CREATE OR REPLACE FUNCTION sp_lst_servicio()
  RETURNS TABLE(_srv_id integer, _srv_data jsonb, _srv_propiedades jsonb) AS
$BODY$
  BEGIN
    RETURN QUERY 
      select srv_id, srv_data, srv_propiedades  from s_servicios
      where srv_estado = 'A' order by srv_id desc, srv_registrado desc;
  END;
  $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION sp_lst_servicio()
  OWNER TO postgres;  

-- DROP FUNCTION sp_baja_servicio(integer);

CREATE OR REPLACE FUNCTION sp_baja_servicio(id_servicio integer)
  RETURNS boolean AS
$BODY$
declare 

tipoServ text;
idworkspace integer;

BEGIN

     select (srv_data->>'srv_tipo_servicio':: text) into tipoServ from s_servicios WHERE srv_id= id_servicio  and srv_estado ='A';
     select srv_workspace_id into idworkspace from s_servicios WHERE srv_id= id_servicio  and srv_estado ='A';

      IF tipoServ = 'C' THEN
      UPDATE s_servicios
       SET srv_estado = 'B' 
       WHERE srv_id= id_servicio  and srv_estado ='A';
       
       UPDATE s_catalogo 
       SET ctp_estado = 'B' 
       WHERE ctp_id= idworkspace  and ctp_estado ='A';
      ELSE
  
          UPDATE s_servicios
    SET srv_estado = 'B' 
    WHERE srv_id= id_servicio  and srv_estado ='A';
      
      END IF;
      
     RETURN true;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION sp_baja_servicio(integer)
  OWNER TO postgres;


CREATE OR REPLACE FUNCTION sp_update_servicio(
    id integer,
    dato jsonb,
    propiedades jsonb,
    estado character,
    usr_id integer)
  RETURNS boolean AS
$BODY$
  DECLARE 
  workpace integer;
  nombre text;
  host text;  
  descripcion text;
  desWork text;
  clasificador text;
  codigo text;
  tipoServ text;
    BEGIN
      IF (estado = 'A') THEN
  
  select (srv_data->> 'srv_tipo_servicio':: text) into tipoServ from s_servicios WHERE srv_id=id;
  IF tipoServ = 'C' THEN
       nombre = propiedades->> 'srv_nombre':: text;
       descripcion = dato->'srv_bd';
       descripcion = REPLACE (descripcion, '"', '') ;
       host = dato->'srv_host';
             host = REPLACE (host, '"', '') ;
       desWork = nombre ||'->' || descripcion || '->' || host;  
             descripcion =  descripcion || '->' || host;  
       SELECT upper(
       SUBSTRING (descripcion, 1, 3))into clasificador;
      
          select srv_workspace_id into workpace from s_servicios WHERE srv_id=62;
  
          UPDATE s_servicios
          SET srv_data=dato, srv_propiedades=propiedades, srv_usr_id=usr_id,srv_modificado=now()
          WHERE srv_id=id;

          UPDATE s_catalogo
          SET ctp_descripcion=desWork, ctp_clasificador=clasificador
          WHERE ctp_id=workpace ; 
        ELSE

        UPDATE s_servicios
        SET srv_data=dato, srv_propiedades=propiedades, srv_usr_id=usr_id,srv_modificado=now()
        WHERE srv_id=id;
        
       END IF;

      END IF;

      IF (estado = 'B')THEN
        UPDATE s_servicios
        SET srv_estado =estado,srv_modificado=now()
        WHERE srv_id=id;
      END IF;
      RETURN true;
    END;
  $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION sp_update_servicio(integer, jsonb, jsonb, character, integer)
  OWNER TO postgres;
  

CREATE OR REPLACE FUNCTION sp_obtener_servicio(IN id integer)
  RETURNS TABLE(_ser_id integer, _ser_tipo_servicio text, _ser_bd text, _ser_gestor text, _ser_puerto text, _ser_host text, _ser_sp text, _ser_endpoint text, _ser_endpoint_verbo text, _ser_argumentos text, _ser_usuario text, _ser_clave text) AS
$BODY$
  BEGIN
    RETURN QUERY 
      select srv_id,(srv_data->> 'srv_tipo_servicio':: text)as ser_tipo_servicio,(srv_data->> 'srv_bd':: text)as ser_bd,(srv_data->> 'srv_gestor':: text)as ser_gestor,(srv_data->> 'srv_puerto':: text)as ser_puerto,
      (srv_data->> 'srv_host':: text)as ser_host,(srv_data->> 'srv_sp':: text)as ser_sp,
      (srv_data->> 'srv_endpoint':: text)as ser_endpoint,(srv_data->> 'srv_endpoint_verbo':: text)as ser_endpoint_verbo,
      (srv_data->> 'srv_argumentos')as ser_argumentos,(srv_data->> 'srv_usuario':: text)as ser_usuario,(srv_data->> 'srv_clave':: text)as ser_clave
       
      from s_servicios 
      where  srv_estado = 'A' and  srv_id = id and (srv_data->> 'srv_tipo_servicio':: text) IN ('S','C')
      order by srv_registrado desc;
  END;
  $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION sp_obtener_servicio(integer)
  OWNER TO postgres;


CREATE OR REPLACE FUNCTION sp_listar_codigo()
  RETURNS TABLE(_scd_id integer, _scd_data jsonb, _scd_propiedades jsonb) AS
$BODY$
  BEGIN
    RETURN QUERY 
      select scd_id,(scd_data->> 'scd_codigo':: text)as cod_codigo,
      (scd_propiedades->> 'scd_orden':: text)as cod_orden,
      (scd_propiedades->> 'scd_nombre':: text)as cod_nombre,
      (scd_propiedades->> 'scd_version':: text)as cod_version,
      (scd_propiedades->> 'scd_descripcion':: text)as cod_descripcion
      from s_codigo 
      where scd_estado = 'A'
      order by scd_registrado desc;
  END;
  $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION sp_listar_codigo()
  OWNER TO postgres;

CREATE OR REPLACE FUNCTION sp_lst_codigo()
  RETURNS TABLE(_scd_id integer, _scd_data jsonb, _scd_propiedades jsonb) AS
$BODY$
  BEGIN
    RETURN QUERY 
      select scd_id, scd_data, scd_propiedades  from s_codigo
      where scd_estado = 'A' order by scd_id desc, scd_registrado desc;
  END;
  $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION sp_lst_codigo()
  OWNER TO postgres;

CREATE OR REPLACE FUNCTION sp_modifica_codigo(
    id integer,
    datos jsonb,
    prop jsonb)
  RETURNS boolean AS
$BODY$
BEGIN
  update s_codigo set scd_data = datos ,scd_propiedades = prop
  where scd_id = id  ;

    RETURN true;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION sp_modifica_codigo(integer, jsonb, jsonb)
  OWNER TO postgres;


CREATE OR REPLACE FUNCTION sp_update_datocodigo(
    id integer,
    datos jsonb)
  RETURNS boolean AS
$BODY$
BEGIN
  update s_codigo set scd_data = datos
  where scd_id = id;
    RETURN true;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION sp_update_datocodigo(integer, jsonb)
  OWNER TO postgres;


CREATE OR REPLACE FUNCTION sp_get_codigo(id integer)
  RETURNS SETOF jsonb AS
$BODY$
  BEGIN
    RETURN QUERY 
      select scd_data from s_codigo where scd_estado like 'A'and scd_id = id;
  END;
  $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION sp_get_codigo(integer)
  OWNER TO postgres;



CREATE OR REPLACE FUNCTION sp_get_codigo_id(IN id integer)
  RETURNS TABLE(rn_id integer, rn_prpiedades jsonb, rn_data jsonb) AS
$BODY$
  BEGIN
    RETURN QUERY 
      select scd_id, scd_propiedades, scd_data from s_codigo where scd_estado = 'A'and scd_id = id;
  END;
  $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION sp_get_codigo_id(integer)
  OWNER TO postgres;

CREATE OR REPLACE FUNCTION sp_lst_regla_nombre()
  RETURNS TABLE(_re_id integer, _re_pro_nombre text, _re_pro_version text,workspace integer) AS
$BODY$
  BEGIN
    RETURN QUERY 
      select scd_id,(scd_propiedades->> 'scd_nombre':: text)as cod_nombre,(scd_propiedades->> 'scd_version':: text)as cod_version ,scd_workspace_id
      from s_codigo 
      where scd_estado = 'A'
      order by scd_registrado desc;
  END;
  $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION sp_lst_regla_nombre()
  OWNER TO postgres;


CREATE OR REPLACE FUNCTION sp_baja_regla_negocio(id_reglaNegocio integer)
  RETURNS boolean AS
$BODY$
BEGIN
     UPDATE s_codigo
     SET scd_estado = 'B' 
     WHERE scd_id= id_reglaNegocio  and scd_estado ='A';
     RETURN true;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION sp_baja_regla_negocio(integer)
  OWNER TO postgres;



CREATE OR REPLACE FUNCTION sp_get_servico_combo(IN tipo text)
  RETURNS TABLE(ser_id integer, ser_nombre text, workspace integer) AS
$BODY$
  BEGIN
    RETURN QUERY 
      select srv_id,(srv_propiedades->> 'srv_nombre':: text)as srv_nombre, srv_workspace_id
      from s_servicios 
      where srv_estado = 'A' and (srv_data->>'srv_tipo_servicio':: text)=tipo
      order by srv_registrado desc;
  END;
  $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION sp_get_servico_combo(text)
  OWNER TO postgres;
-----------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------

-- Realizar un alter table de la tabla s_servicios añadimos el campo srv_workspace_id:
   ALTER TABLE s_servicios add srv_workspace_id integer

-- Realizar un alter table de la tabla s_codigo añadimos el campo scd_workspace_id:
   ALTER TABLE s_codigo add scd_workspace_id integer
-----------------------------------------------------------------------------------------------------------------
-- creamos la tabla s_catalogo para las parametricas:

CREATE TABLE s_catalogo
(
  ctp_id serial NOT NULL,
  ctp_descripcion character varying(200) NOT NULL,
  ctp_clasificador character varying(200) NOT NULL,
  ctp_codigo character varying(20) NOT NULL,
  ctp_usr_id integer,
  ctp_registrado timestamp without time zone NOT NULL DEFAULT now(),
  ctp_modificado timestamp without time zone NOT NULL DEFAULT now(),
  ctp_estado character(1) NOT NULL DEFAULT 'A'::bpchar,
  CONSTRAINT s_catalogo_pkey PRIMARY KEY (ctp_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE s_catalogo
  OWNER TO postgres;


INSERT INTO s_catalogo (ctp_id, ctp_descripcion, ctp_clasificador, ctp_codigo, ctp_usr_id, ctp_registrado, ctp_modificado, ctp_estado) VALUES 
(0, '-- Seleccione --', 'select', '0', 1, '2017-12-15 11:36:57.166603', '2017-12-15 11:36:57.166603', 'A');
-----------------------------------------------------------------------------------------------------------------
--Eliminar la anterior funcion sp_insert_regla:

-- DROP FUNCTION sp_insert_regla(jsonb, jsonb, integer);

-- Crear la nueva funcion sp_insert_regla el cual se modifico añadiendo el campo workspace:

CREATE OR REPLACE FUNCTION sp_insert_regla(
    datos jsonb,
    propiedades jsonb,
    usr_id integer,
    workspace integer)
  RETURNS boolean AS
$BODY$
    BEGIN
      INSERT INTO s_codigo(scd_data, scd_propiedades, scd_usr_id, scd_workspace_id)VALUES (datos,propiedades,usr_id, workspace);
      RETURN true;
    END;
  $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION sp_insert_regla(jsonb, jsonb, integer, integer)
  OWNER TO postgres;

-----------------------------------------------------------------------------------------------------------------

-- Function: sp_insert_servicio(jsonb, jsonb, integer, integer, text)

-- DROP FUNCTION sp_insert_servicio(jsonb, jsonb, integer, integer, text);

CREATE OR REPLACE FUNCTION sp_insert_servicio(
    datos jsonb,
    propiedades jsonb,
    usr_id integer,
    workspace integer,
    tiposerv text)
  RETURNS boolean AS
$BODY$
DECLARE 
  nombre text;
  host text;  
  descripcion text;
  desWork text;
  clasificador text;
  codigo text;
  tipo text;
    BEGIN
    nombre = propiedades->> 'srv_nombre':: text;
    descripcion = datos->'srv_bd';
    descripcion = REPLACE (descripcion, '"', '') ;
    host = datos->'srv_host';
    host = REPLACE (host, '"', '') ;
    desWork = nombre ||'->' || descripcion || '->' || host; 
    descripcion =  descripcion || '->' || host; 
    
    SELECT upper(
    SUBSTRING (descripcion, 1, 3))into clasificador;

      IF tipoServ = 'C' THEN
     select ctp_id + 1 into codigo from s_catalogo order by ctp_id desc limit 1;  
     INSERT INTO s_servicios(srv_data, srv_propiedades, srv_usr_id,srv_workspace_id)VALUES (datos, propiedades, usr_id, (codigo)::integer);   
           INSERT INTO s_catalogo(ctp_descripcion, ctp_usr_id, ctp_clasificador, ctp_codigo)VALUES (desWork,usr_id, clasificador,codigo);
      ELSE
  
         INSERT INTO s_servicios(srv_data, srv_propiedades, srv_usr_id,srv_workspace_id)VALUES (datos, propiedades, usr_id, workspace); 
      END IF;
  
      RETURN true; 
    END;
  $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION sp_insert_servicio(jsonb, jsonb, integer, integer, text)
  OWNER TO postgres;

-----------------------------------------------------------------------------------------------------------------
-- Creamos la funcion sp_lst_codigo_rn:

CREATE OR REPLACE FUNCTION sp_lst_codigo_rn(IN idworkspace integer)
  RETURNS TABLE(_scd_id integer, _scd_orden text, _scd_nombre text, _scd_version text, _scd_descripcion text, _scd_data jsonb) AS
$BODY$
  BEGIN
    RETURN QUERY 
      select scd_id,
      (scd_propiedades->> 'scd_orden':: text)as cod_orden,
      (scd_propiedades->> 'scd_nombre':: text)as cod_nombre,
      (scd_propiedades->> 'scd_version':: text)as cod_version,
      (scd_propiedades->> 'scd_descripcion':: text)as cod_descripcion,
      scd_data
      from s_codigo 
      where scd_estado = 'A' and idworkspace = scd_workspace_id order by scd_id desc, scd_registrado desc;
  END;
  $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION sp_lst_codigo_rn(integer)
  OWNER TO postgres;

-----------------------------------------------------------------------------------------------------------------
-- Eliminar la anterior funcion sp_lst_codigo2:
-- DROP FUNCTION sp_lst_codigo2();

-- Creamos la nueva funcion sp_lst_codigo2
CREATE OR REPLACE FUNCTION sp_lst_codigo2(IN value integer)
  RETURNS TABLE(_scd_id integer, _scd_orden text, _scd_nombre text, _scd_version text, _scd_descripcion text, _scd_data jsonb) AS
$BODY$
  BEGIN
    RETURN QUERY 
      select scd_id,
      (scd_propiedades->> 'scd_orden':: text)as cod_orden,
      (scd_propiedades->> 'scd_nombre':: text)as cod_nombre,
      (scd_propiedades->> 'scd_version':: text)as cod_version,
      (scd_propiedades->> 'scd_descripcion':: text)as cod_descripcion,
      scd_data
      from s_codigo 
      where scd_estado = 'A' and srv_workspace_id = value order by scd_id desc, scd_registrado desc;
  END;
  $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION sp_lst_codigo2(integer)
  OWNER TO postgres;
-----------------------------------------------------------------------------------------------------------------
-- Eliminar la anterior funcion sp_lst_servicio2():
-- DROP FUNCTION sp_lst_servicio2();

-- Creamos la nueva funcion sp_lst_servicio2(IN idworkspace integer)

CREATE OR REPLACE FUNCTION sp_lst_servicio2(IN idworkspace integer)
  RETURNS TABLE(_srv_id integer, _srv_nombre text, _srv_descripcion text, _srv_endpoint text, _srv_verbo text, _srv_sp text, _srv_bd text, _srv_tipo_servicio text, _srv_argumentos text) AS
$BODY$
  BEGIN
    RETURN QUERY 
      select srv_id,
      (srv_propiedades->> 'srv_nombre':: text)as cod_nombre,
      (srv_propiedades->> 'srv_descripcion':: text)as cod_descripcion,
      (srv_data->> 'srv_endpoint':: text)as cod_endpoint,
      (srv_data->> 'srv_endpoint_verbo':: text)as cod_endpoint_verbo,
      (srv_data->> 'srv_sp':: text)as cod_sp,
      (srv_data->> 'srv_bd':: text)as cod_bd,
      (srv_data->> 'srv_tipo_servicio':: text)as cod_tipo_servicio,
      (srv_data->> 'srv_argumentos':: text)as cod_argumentos
      from s_servicios 
      where srv_estado = 'A' and idworkspace = srv_workspace_id order by srv_id desc, srv_registrado desc;
  END;
  $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION sp_lst_servicio2(integer)
  OWNER TO postgres;
-----------------------------------------------------------------------------------------------------------------
--SELECT pg_catalog.setval('s_catalogo_ctp_id_seq', 2, true);
-- sp_insert_servicio actualizar esta funcion...
