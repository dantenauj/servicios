ALTER TABLE s_codigo ADD scd_identificador text NOT NULL default '';

--ALTER TABLE s_codigo DROP COLUMN scd_identificador  


ALTER TABLE s_servicios ADD srv_id_incremento integer NOT NULL default 1;

--ALTER TABLE s_servicios DROP COLUMN srv_id_incremento  

-- Function: public.sp_get_codigo(text)

-- DROP FUNCTION public.sp_get_codigo(text);

CREATE OR REPLACE FUNCTION public.sp_get_codigo(tipificador text)
  RETURNS SETOF jsonb AS
$BODY$
  BEGIN
    RETURN QUERY 
      select scd_data from s_codigo where scd_estado like 'A'and scd_identificador = tipificador;
  END;
  $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION public.sp_get_codigo(text)
  OWNER TO postgres;

-- Function: public.sp_identificador(integer)

-- DROP FUNCTION public.sp_identificador(integer);

CREATE OR REPLACE FUNCTION public.sp_identificador(workspace integer)
  RETURNS text AS
$BODY$
   DECLARE
   clasificador text;
   incremento integer;
   identificador text;
    BEGIN

  select (srv_data->>'srv_bd'::text) into clasificador from s_servicios where srv_data->>'srv_tipo_servicio' = 'C'and srv_workspace_id = workspace;
  select (srv_id_incremento) into incremento from s_servicios where srv_data->>'srv_tipo_servicio' = 'C'and srv_workspace_id = workspace;

  --update s_servicios set srv_id_incremento = incremento+1 where srv_data->>'srv_tipo_servicio' = 'C'and srv_workspace_id = workspace;
  
      RETURN  clasificador ||'-'||incremento;
    END;
  $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public.sp_identificador(integer)
  OWNER TO postgres;


-- Function: public.sp_get_codigo_idrn(integer)

-- DROP FUNCTION public.sp_get_codigo_idrn(integer);

CREATE OR REPLACE FUNCTION public.sp_get_codigo_idrn(id integer)
  RETURNS SETOF jsonb AS
$BODY$
  BEGIN
    RETURN QUERY 
      select scd_data from s_codigo where scd_estado like 'A'and scd_id = id;
  END;
  $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION public.sp_get_codigo_idrn(integer)
  OWNER TO postgres;

---eliminar la tabla
-- DROP FUNCTION public.sp_get_codigo(integer);

  ----Modificaciones


 DROP FUNCTION public.sp_insert_regla(jsonb, jsonb, integer, integer);

CREATE OR REPLACE FUNCTION public.sp_insert_regla(
    datos jsonb,
    propiedades jsonb,
    usr_id integer,
    workspace integer,
    ident text)
  RETURNS boolean AS
$BODY$
   DECLARE
   clasificador text;
   incremento integer;
   identificador text;
    BEGIN
  select (srv_id_incremento) into incremento from s_servicios where srv_data->>'srv_tipo_servicio' = 'C'and srv_workspace_id = workspace;
  update s_servicios set srv_id_incremento = incremento+1 where srv_data->>'srv_tipo_servicio' = 'C'and srv_workspace_id = workspace;
  
      INSERT INTO s_codigo(scd_data, scd_propiedades, scd_usr_id, scd_workspace_id, scd_identificador)VALUES (datos,propiedades,usr_id, workspace,ident);
      RETURN true;
    END;
  $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public.sp_insert_regla(jsonb, jsonb, integer, integer, text)
  OWNER TO postgres;

DROP FUNCTION public.sp_lst_codigo_rn(integer);

CREATE OR REPLACE FUNCTION public.sp_lst_codigo_rn(IN idworkspace integer)
  RETURNS TABLE(_scd_id integer, _scd_orden text, _scd_nombre text, _scd_version text, _scd_descripcion text, _scd_data jsonb, identificador text) AS
$BODY$
  BEGIN
    RETURN QUERY 
      select scd_id,
      (scd_propiedades->> 'scd_orden':: text)as cod_orden,
      (scd_propiedades->> 'scd_nombre':: text)as cod_nombre,
      (scd_propiedades->> 'scd_version':: text)as cod_version,
      (scd_propiedades->> 'scd_descripcion':: text)as cod_descripcion,
      scd_data,
      scd_identificador
      from s_codigo 
      where scd_estado = 'A' and idworkspace = scd_workspace_id order by scd_id desc, scd_registrado desc;
  END;
  $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION public.sp_lst_codigo_rn(integer)
  OWNER TO postgres;

DROP FUNCTION public.sp_lst_regla_nombre();

CREATE OR REPLACE FUNCTION public.sp_lst_regla_nombre()
  RETURNS TABLE(_re_id integer, _re_pro_nombre text, _re_pro_version text, workspace integer, identificador text) AS
$BODY$
  BEGIN
    RETURN QUERY 
      select scd_id,(scd_propiedades->> 'scd_nombre':: text)as cod_nombre,(scd_propiedades->> 'scd_version':: text)as cod_version ,scd_workspace_id,scd_identificador
      from s_codigo 
      where scd_estado = 'A'
      order by scd_registrado desc;
  END;
  $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION public.sp_lst_regla_nombre()
  OWNER TO postgres;

DROP FUNCTION public.sp_modifica_codigo(integer, jsonb, jsonb);

CREATE OR REPLACE FUNCTION public.sp_modifica_codigo(
    id integer,
    datos jsonb,
    prop jsonb,
    identificador text)
  RETURNS boolean AS
$BODY$
BEGIN
  update s_codigo set scd_data = datos ,scd_propiedades = prop , scd_identificador = identificador
  where scd_id = id  ;

    RETURN true;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public.sp_modifica_codigo(integer, jsonb, jsonb, text)
  OWNER TO postgres;

  DROP FUNCTION public.sp_get_codigo_id(integer);

CREATE OR REPLACE FUNCTION public.sp_get_codigo_id(IN id integer)
  RETURNS TABLE(rn_id integer, rn_prpiedades jsonb, rn_data jsonb, identificador text) AS
$BODY$
  BEGIN
    RETURN QUERY 
      select scd_id, scd_propiedades, scd_data ,scd_identificador from s_codigo where scd_estado = 'A'and scd_id = id;
  END;
  $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION public.sp_get_codigo_id(integer)
  OWNER TO postgres;


