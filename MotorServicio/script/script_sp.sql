
CREATE OR REPLACE FUNCTION autenticacion(
    IN usrid text,
    IN usuario text)
  RETURNS TABLE(idusr integer, usr text, nombre text, paterno text, materno text, idrl integer, rol text, vci text, vprs_id integer) AS
$BODY$
BEGIN
    RETURN QUERY SELECT usr_id,usr_usuario,prs_nombres,prs_paterno,prs_materno,rls_id,rls_rol,prs_ci,prs_id
     FROM _bp_usuarios
     INNER JOIN _bp_personas ON  prs_id=usr_prs_id  and prs_estado='A'
     INNER JOIN _bp_usuarios_roles ON usrls_usr_id=usr_id  and usrls_estado='A'
     INNER  JOIN _bp_roles ON usrls_rls_id=rls_id and rls_estado='A'
     where usr_usuario=usrId  AND usr_clave=usuario AND usr_estado='A' ORDER BY usrls_registrado DESC ;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

  CREATE OR REPLACE FUNCTION sp_obtiene_usuario(
    IN usrid INTEGER)
  RETURNS TABLE(vrls_id INTEGER,vrls_rol TEXT,vusr_usuario TEXT,vprs_nombres TEXT ,vprs_paterno TEXT,vprs_materno TEXT,vprs_id INTEGER) AS
$BODY$
BEGIN
  RETURN QUERY select rls_id,rls_rol,usr_usuario,prs_nombres ,prs_paterno ,prs_materno ,prs_id
  from _bp_usuarios_roles
  inner join _bp_roles as r on r.rls_id= usrls_rls_id and usrls_estado='A' AND r.rls_estado='A'
  inner join _bp_usuarios as u on u.usr_id=usrls_usr_id AND usr_estado<> 'B'
  inner join _bp_personas as p on p.prs_id=u.usr_prs_id AND prs_estado='A'
  where usrls_usr_id=usrid;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

  CREATE OR REPLACE FUNCTION sp_lst_catalogo()
  RETURNS TABLE(id_ctp integer, descripcion character varying) AS
$BODY$
  BEGIN
    RETURN QUERY 
      select ctp_id, ctp_descripcion
      from s_catalogo
      where ctp_estado = 'A' order by ctp_id asc;
      
  END;
  $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION sp_lst_catalogo()
  OWNER TO postgres;





