<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## Acerca del Motor de Servicios version laravel 5.5.21 actualizado 17 de noviembre del 2017


1.- Instalar librerias composer ejecutando el comando en modo super usuario
composer install o composer update.

Ademas debera cambiar en el archivo EloquentUserProvider de la carpeta Vendor las variables de pasword por usr_clave en las funciones 
	-- retrieveByCredentials
    -- validateCredentials

2.- Despues de ejecutar el comando composer install o composer update se descargara un archivo vendor con las librerias necesarias para el inicio del sistema.

3.- La version de php debe ser mayor al 7.0 para el arranque del sistema.

4.- La parte de los controladores se encuentra dividido por las carpetas
			Auth
			admin         => se encuentran la parte administrativa del proyecto. 
			parametricas  => se encuentran las funciones de los submodulos del proyecto.
			servicios     => Se encuentran funciones de los servicios declarados en el archivo route.

5.- La carpeta routes se encuentra dividido en subarchivos
			api.php       => Es donde se declara las rutas de los servicios que tiene seguridad de Token.
			web.php       => Es donde se declara las rutas de los servicios 

6.- La parte de las vistas se encuentra dividida por 
            backend      => Se encuentra la parte de la administracion, template
            errores		 => Se enuntran los diferentes tipos de error
            frontend     => Se encuentra la parte de auth, bienvenida y el login

7.- Solo subir los archivos necesarios sin el vendor al repositorio.

